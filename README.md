# medical-image

## 설치

1. [Node](https://nodejs.org/ko/download/), [yarn](https://yarnpkg.com/en/docs/install#windows-stable)을 설치합니다.
2. 저장소를 클론합니다. 깃 계정과 비밀번호를 입력해야 합니다.
   ```
   git clone http://freeive.iptime.org/Sunghyun/medical-image.git
   ```
3. 모듈을 다운로드합니다.
   ```
   yarn install
   ```
4. `webpack` 명령을 실행시켜 scss, js 파일을 빌드합니다.\
   `webpack-watch`명령을 실행시키면 파일이 변경될 때마다 자동으로 빌드됩니다.
   ```
   yarn webpack
   ```
5. 따로 공유해 드린 `.env`파일을 프로젝트 디렉토리의 루트 경로에 복사합니다.
6. `dev`명령을 실행시켜 개발 서버를 실행합니다.
   ```
   yarn dev
   ```
7. http://localhost:3000/ 에 접속하여 확인합니다.

## 배포

자동 배포 환경을 구축해 놓지 않아, 수동으로 원격 서버에 접속하여 배포합니다.

1. 윈도우의 [원격 데스크톱 연결]을 실행시키고 `13.124.209.7`에 접속합니다.
2. 최초로 배포할 경우, 커맨드 창에서 설치 방법과 같은 방법으로 진행하시면 됩니다.
3. 변경 사항을 적용할 때는
   ```
   git pull origin [브랜치명]
   ```
   으로 변경사항을 내려받은 뒤 `webpack-watch`를 실행시키고 `dev`명령어를 실행시킵니다.

P.S 젠킨스를 사용해보려고 했지만... 도저히 윈도우 서버에서 어떻게 하는지 모르겠습니다. 죄송합니다 T_T

## 추가 설명

### 사용자 지정 명령어

- package.json 파일에 사용자 지정 스크립트가 있습니다. 스크립트 파일의 명령어들은 다음과 같습니다.

  ```
  "scripts": {
    "dev": "nodemon --ignore sessions/ --exec ts-node src/app.ts",
    "webpack": "webpack",
    "webpack-watch": "webpack -w"
  }
  ```

  - `dev`: ts-node 모듈을 이용해서 타입스크립트 문법을 해석하고, nodemon 모듈을 이용해서 app.js를 실행시킵니다.
  - `webpack`: webpack으로 assets 디렉토리에 위치한 scss파일과 js파일을 빌드합니다.
  - `webpack-watch`: assets 디렉토리에 위치한 scss파일과 js파일이 변경될 때마다 자동으로 빌드합니다.

### 설정 파일

- 프로젝트를 실행시킬 때 설정파일(.env)이 필요합니다.
- 설정파일에는 DB정보, AWS엑세스 키 등 중요 정보가 들어 있어 git에 업로드하지 않고 따로 공유해 드리겠습니다.
- 프로젝트 디렉토리의 최상위에 .env 파일을 생성해서 공유한 내용을 입력해 주세요.

### 사진 저장소

- 사진은 [AWS S3](https://aws.amazon.com/ko/)에 저장됩니다.
- 저장 경로는 `sqiengbucket/medical-image`입니다.
