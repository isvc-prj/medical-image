import 'reflect-metadata';

import * as dotenv from 'dotenv';
dotenv.config();

import { createConnection } from 'typeorm';
import * as express from 'express';
import * as path from 'path';
import * as passport from 'passport';
import * as mogran from 'morgan';
import * as session from 'express-session';
import * as cors from 'cors';
const FileStore = require('session-file-store')(session);

import { localMiddleware } from './middlewares';
import passportConfig from './config/passport';
import entities from './entity';
import { appRoutes } from './routes';

const { DB_HOST, DB_USERNAME, DB_PASSWORD, DB_PORT, SID } = process.env;

createConnection({
  type: 'oracle',
  host: DB_HOST,
  username: DB_USERNAME,
  password: DB_PASSWORD,
  port: parseInt(DB_PORT),
  sid: SID,
  synchronize: false,
  entities: [...entities],
  dropSchema: false
})
  .then(async () => {
    const app: express.Application = express();

    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(
      session({
        secret: 'secret',
        resave: false,
        saveUninitialized: false,
        store: new FileStore()
      })
    );
    app.use(cors());
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(mogran('dev'));
    app.use(localMiddleware);
    app.use(express.static(path.join(__dirname, '..', 'static')));
    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'pug');
    passportConfig();

    appRoutes.forEach(({ path, method, handlers }) => {
      app[method](path, handlers);
    });

    app.listen(process.env.PORT || 3000, () => {
      console.log(`server run http://localhost:${process.env.PORT || 3000}`);
    });
  })
  .catch(error => console.log(error));
