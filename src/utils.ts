import moment = require('moment');
import * as querystring from 'querystring';
import { getManager } from 'typeorm';

export const getHospitalsOfUser = async (userId: string) => {
  return await getManager().query(
    `SELECT HSPTL_ID as "id", FN_GET_HSPTL_NM(HSPTL_ID) as "name" FROM USR_HSPTL_MAP WHERE USR_ID = '${userId}' ORDER BY SORT_SEQ`
  );
};

export const getDiagnosisInfo = async (diagnosisId: string) => {
  if (diagnosisId) {
    const diagnosisInfo = await getManager().query(`
              SELECT
                FN_GET_DGNS_NM(${diagnosisId}) as "name",
                FN_GET_ICD10_CD(${diagnosisId}) as "ICD10"
              FROM dual`);
    return diagnosisInfo.length !== 0 ? diagnosisInfo[0] : {};
  } else return {};
};

export const removeDateSep = (date: string) =>
  date
    .replace(/-/gi, '')
    .replace(/ /gi, '')
    .replace(/:/gi, '');

export const convertDate = (date: string) =>
  moment(date, 'YYYYMMDD').format('YYYY-MM-DD');

export const convertDatetime = (datetime: string) =>
  moment(datetime, 'YYYYMMDDhhmmss').format('YYYYMMDD hh:mm:ss');

export const createQueryUrl = (path: string, query?: Object) => {
  const _query = JSON.parse(JSON.stringify(query));
  delete _query['page'];
  return `${path}?${querystring.stringify(_query)}`;
};

const getOriginalname = (name: string) => {
  let [originalname, extension] = name.split('.');
  if (originalname.indexOf('/') !== -1) {
    originalname = originalname.split('/')[1];
  }

  return originalname;
};

export const getHospitalId = (name: string) => {
  const [hospitalId, num] = getOriginalname(name).split('-');
  return hospitalId;
};

export const getPatientNum = (name: string) => {
  let [hospitalId, num] = getOriginalname(name).split('-');
  if (num.indexOf('(') !== -1) num = num.split(' ')[0];
  return num;
};

export const getPagination = (curPage: number = 1, totalCount: number) => {
  curPage = Number(curPage);
  const maxPageInSet = 5,
    maxEntityInPage = 10;
  const totalPage = Math.ceil(totalCount / maxEntityInPage),
    totalSet = Math.ceil(totalPage / maxPageInSet),
    curSet = Math.ceil(curPage / maxPageInSet),
    startPage = (curSet - 1) * maxPageInSet + 1,
    lastPage = startPage + maxPageInSet - 1,
    prevPage = startPage - 1,
    nextPage = lastPage + 1;

  return {
    totalPage,
    totalSet,
    curPage,
    startPage,
    lastPage,
    prevPage,
    nextPage
  };
};

export const getTodayDate = () => '2019-03-04'; //moment(new Date()).format('YYYY-MM-DD');
