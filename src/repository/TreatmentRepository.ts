import { EntityRepository, Repository } from 'typeorm';
import { Treatment } from '../entity/Treatment';

@EntityRepository(Treatment)
export class TreatmentRepository extends Repository<Treatment> {
  async getList(
    startAt: string,
    endAt: string,
    hospitalId: string,
    diagnosisId: string,
    page: number = 1,
    limit: number = 10
  ) {
    const sql = `
      SELECT 
        HSPTL_ID as "hospitalId",
        FN_GET_HSPTL_NM(HSPTL_ID) as "hospitalName",
        TO_CHAR(TO_DATE(TRTMT_YMD,'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
        PTNT_NO as "patientNum",
        FN_GET_CD_NM('GNDR_CD', GNDR_CD) as "gender",
        PTNT_AGE as "age",
        FN_GET_DGNS_NM(DGNS_ID) as "diagnosisName",
        FN_GET_ICD10_CD(DGNS_ID) as "ICD10",
        EXCLD_DGNS_NM as "exclusionDiagnosisName",               
        INTRST_RGN as "interestRegion",                                                         
        PHTO_REG_CNT as "photoCount",
        TO_CHAR(TO_DATE(REG_DT,'YYYYMMDDHH24MISS'), 'YYYY-MM-DD HH24:MI:SS') as "regDate",
        RMK as "rmk"
      FROM TRTMT_MST
      WHERE TO_DATE(TRTMT_YMD,'YYYYMMDDHH24MISS') >= TO_DATE('${startAt}','YYYY-MM-DD')
        AND TO_DATE(TRTMT_YMD,'YYYYMMDDHH24MISS') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
        AND HSPTL_ID = '${hospitalId}'
        ${diagnosisId ? `AND DGNS_ID=${diagnosisId}` : ''}
      ORDER BY PTNT_NO
      OFFSET ${(page - 1) * limit} ROWS FETCH NEXT ${limit} ROWS ONLY`;

    const list = await this.query(sql);

    const count = await this.getCount(startAt, endAt, hospitalId, diagnosisId);

    return { list, count };
  }

  async getCount(
    startAt: string,
    endAt: string,
    hospitalId: string,
    diagnosisId?: string
  ) {
    try {
      return (await this.query(`
        SELECT COUNT(DISTINCT PTNT_NO||'-'||HSPTL_ID||'-'||TRTMT_YMD) AS "cnt"
          FROM TRTMT_MST
          WHERE TO_DATE(TRTMT_YMD, 'YYYYMMDD') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
            AND TO_DATE(TRTMT_YMD, 'YYYYMMDD') >= TO_DATE('${startAt}','YYYY-MM-DD')
            AND HSPTL_ID = '${hospitalId}'
            ${diagnosisId ? `AND DGNS_ID=${diagnosisId}` : ''}`))[0]['cnt'];
    } catch (error) {
      console.error(error);
      return 0;
    }
  }

  async getListWithPatient(
    startAt: string,
    endAt: string,
    hospitalId: string,
    limit: number = 10,
    page: number = 1
  ) {
    try {
      return await this.query(`
      SELECT 
        FN_GET_HSPTL_NM(A.HSPTL_ID) as "hospitalName",
        TO_CHAR(TO_DATE(A.TRTMT_YMD,'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
        A.PTNT_NO as "patientNum",
        FN_GET_CD_NM('GNDR_CD', A.GNDR_CD ) as "gender",
        A.PTNT_AGE as "age",
        FN_GET_DGNS_NM(A.DGNS_ID) as "diagnosisName",
        A.PHTO_REG_CNT as "photoCount"
      FROM TRTMT_MST A
      WHERE TO_DATE(A.TRTMT_YMD,'YYYYMMDDHH24MISS') >= TO_DATE('${startAt}','YYYY-MM-DD')
        AND TO_DATE(A.TRTMT_YMD, 'YYYYMMDDHH24MISS') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
        AND A.HSPTL_ID  = '${hospitalId}'
      OFFSET ${(page - 1) * limit} ROWS FETCH NEXT ${limit} ROWS ONLY`);
    } catch (error) {
      console.error(error);
      return [];
    }
  }

  async getOne(treatmentDate: string, patientNum: string, hospitalId: string) {
    try {
      return (await this.query(`
        SELECT
          TO_CHAR(TO_DATE(TRTMT_YMD, 'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
          HSPTL_ID as "hospitalId",
          PTNT_NO as "patientNum",
          GNDR_CD as "genderCode",
          FN_GET_CD_NM('GNDR_CD', GNDR_CD) as "gender",
          PTNT_AGE as "age",
          DGNS_ID "diagnosisId",
          FN_GET_DGNS_NM(DGNS_ID) as "diagnosisName",
          FN_GET_ICD10_CD(DGNS_ID) as "ICD10",
          EXCLD_DGNS_NM as "exclusionDiagnosisName",
          INTRST_RGN as "interestRegion",
          REG_DT as "regDate",
          REG_ID as "regId",
          MOD_DT as "modDate",
          MOD_ID as "modId",
          RMK as "rmk",
          CHIF_CMPLNT_CD as "chiefComplain",
          FN_GET_CD_NM('CHIF_CMPLNT_CD', CHIF_CMPLNT_CD) as "chiefComplainName",
          CNFRM_YN as "confirmed"
        FROM TRTMT_MST
        WHERE TRTMT_YMD='${treatmentDate}'
          AND PTNT_NO='${patientNum}'
          AND HSPTL_ID='${hospitalId}'
      `))[0];
    } catch (error) {
      console.error(error);
      return {};
    }
  }

  async getCountWithPatient(date: string, hospitalId: string) {
    try {
      const treatmentCount = (await this.query(`
        SELECT COUNT(DISTINCT PTNT_NO||'-'||HSPTL_ID||'-'||TRTMT_YMD) AS "cnt"
          FROM TRTMT_MST
        WHERE TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') <= TO_DATE('${date} 23:59:59','YYYY-MM-DD HH24:MI:SS')
          AND TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') >= TO_DATE('${date}','YYYY-MM-DD')
          AND DGNS_ID IS NOT NULL
          AND HSPTL_ID = '${hospitalId}'`))[0]['cnt'];

      const patientCount = (await this.query(`
        SELECT COUNT(DISTINCT PTNT_NO||'-'||HSPTL_ID||'-'||TRTMT_YMD) AS "cnt"
          FROM TRTMT_MST
        WHERE TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') <= TO_DATE('${date} 23:59:59','YYYY-MM-DD HH24:MI:SS')
          AND TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') >= TO_DATE('${date}','YYYY-MM-DD')
          AND HSPTL_ID  = '${hospitalId}'`))[0]['cnt'];
      return { treatmentCount, patientCount };
    } catch (error) {
      console.error(error);
      return {};
    }
  }
}
