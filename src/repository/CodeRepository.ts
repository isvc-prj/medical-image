import { Code } from '../entity/Code';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(Code)
export class CodeRepository extends Repository<Code> {
  findClinicalCode() {
    return this.find({
      where: { group: 'CLNC_CLSF_CD' },
      order: {
        sortSequence: 'ASC'
      }
    });
  }
}
