import { EntityRepository, Repository } from 'typeorm';
import { Photo } from '../entity/Photo';

@EntityRepository(Photo)
export class PhotoRepository extends Repository<Photo> {
  async getList(
    startAt: string,
    endAt: string,
    hospitalId: string,
    limit: number = 10,
    page: number = 1,
    diagnosisId,
    description
  ): Promise<any> {
    const sql = `
      SELECT
        ORG_FILE_PATH as "originalFile",
        EDT_FILE_PATH as "editFile",
        A.HSPTL_ID as "hospitalId",
        FN_GET_HSPTL_NM(A.HSPTL_ID) as "hospitalName",
        TO_CHAR(TO_DATE(A.TRTMT_YMD,'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
        A.PTNT_NO as "patientNum",
        B.PHTO_NO as "photoNum",
        TO_CHAR(TO_DATE(B.SHT_DT,'YYYYMMDDHH24MISS'), 'YYYY-MM-DD HH24:MI:SS') as "shootingDate",
        B.SHT_EQPMT_CD as "shootingEquipment",
        FN_GET_BDY_PRT_NM(B.BDY_PRT_ID) as "bodyPartName",
        B.OPRTN_PHOT_YN as "isOperation",
        B.BPSY_PHOT_YN as "isBiopsy",
        B.LSR_PHOT_YN as "isLaser",
        FN_GET_CD_NM('TRTMT_PRGRS_CD', B.TRTMT_PRGRS_CD) as "treatmentProgressName"
      FROM TRTMT_MST A, PHTO_HST B
      WHERE TO_DATE(A.TRTMT_YMD,'YYYYMMDDHH24MISS') >= TO_DATE('${startAt}','YYYY-MM-DD')
        AND TO_DATE(A.TRTMT_YMD,'YYYYMMDDHH24MISS') <= TO_DATE('${endAt}235959','YYYY-MM-DDHH24MISS') 
        AND A.HSPTL_ID = '${hospitalId}'
        AND A.TRTMT_YMD = B.TRTMT_YMD(+)
        AND A.HSPTL_ID = B.HSPTL_ID(+)
        AND A.PTNT_NO = B.PTNT_NO(+)
        AND A.PHTO_REG_CNT != 0
        ${diagnosisId ? `AND A.DGNS_ID = '${diagnosisId}'` : ''}
        ${
          description
            ? `AND 0 != (SELECT COUNT(LBL_NO) FROM LBL_HST WHERE LBL_DESC LIKE '%${description}%' AND LBL_HST.PHOT_NO = B.PHTO_NO)`
            : ''
        }
      ORDER BY A.PTNT_NO
      OFFSET ${(page - 1) * limit} ROWS FETCH NEXT ${limit} ROWS ONLY`;

    try {
      const photos = await this.query(sql);
      const count = await this.getCountWithCondition(
        startAt,
        endAt,
        hospitalId,
        diagnosisId,
        description
      );

      return { photos, count };
    } catch (error) {
      console.error(error);
      return { photos: [], count: 0 };
    }
  }

  async getCount(startAt: string, endAt: string, hospitalId: string) {
    try {
      return (await this.query(
        `SELECT COUNT(PHTO_NO) AS "cnt"
          FROM PHTO_HST
        WHERE TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
          AND TO_DATE(TRTMT_YMD, 'YYYYMMDDHH24MISS') >= TO_DATE('${startAt}','YYYY-MM-DD')
          AND HSPTL_ID = '${hospitalId}'`
      ))[0]['cnt'];
    } catch (error) {
      console.error(error);
      return 0;
    }
  }

  async getCountWithCondition(
    startAt: string,
    endAt: string,
    hospitalId: string,
    diagnosisId: string,
    description: string
  ) {
    try {
      return (await this.query(`
        SELECT
          COUNT(PHTO_NO) AS "cnt"
        FROM TRTMT_MST A, PHTO_HST B
        WHERE TO_DATE(A.TRTMT_YMD,'YYYYMMDDHH24MISS') >= TO_DATE('${startAt}','YYYY-MM-DD')
          AND TO_DATE(A.TRTMT_YMD,'YYYYMMDDHH24MISS') <= TO_DATE('${endAt}235959','YYYY-MM-DDHH24MISS') 
          AND A.HSPTL_ID = '${hospitalId}'
          AND A.TRTMT_YMD = B.TRTMT_YMD(+)
          AND A.HSPTL_ID = B.HSPTL_ID(+)
          AND A.PTNT_NO = B.PTNT_NO(+)
          AND A.PHTO_REG_CNT != 0
          ${diagnosisId ? `AND A.DGNS_ID = '${diagnosisId}'` : ''}
          ${
            description
              ? `AND 0 != (SELECT COUNT(LBL_NO) FROM LBL_HST WHERE LBL_DESC LIKE '%${description}%' AND LBL_HST.PHOT_NO = B.PHTO_NO)`
              : ''
          }`))[0]['cnt'];
    } catch (error) {
      console.error(error);
      return 0;
    }
  }

  async getOneWithLabels(photoNum: string) {
    try {
      const photo = (await this.query(
        `SELECT
          TO_CHAR(TO_DATE(PHTO_HST.TRTMT_YMD, 'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
          PHTO_HST.HSPTL_ID as "hospitalId",
          PHTO_HST.PHTO_NO as "photoNum",
          PHTO_HST.PTNT_NO as "patientNum",
          TO_CHAR(TO_DATE(PHTO_HST.SHT_DT, 'YYYYMMDDHH12MISS'), 'YYYY-MM-DD HH12:MI:SS') as "shootingDate",
          PHTO_HST.SHT_EQPMT_CD as "shootingEquipment",
          PHTO_HST.BDY_PRT_ID as "bodyPartId",
          FN_GET_BDY_PRT_NM(PHTO_HST.BDY_PRT_ID) as "bodyPartName",
          PHTO_HST.OPRTN_PHOT_YN as "isOperation",
          PHTO_HST.BPSY_PHOT_YN as "isBiopsy",
          PHTO_HST.LSR_PHOT_YN as "isLaser",
          PHTO_HST.TRTMT_PRGRS_CD as "treatmentProgress",
          FN_GET_CD_NM('TRTMT_PRGRS_CD', PHTO_HST.TRTMT_PRGRS_CD) as "treatmentProgressName",
          PHTO_HST.ORG_FILE_PATH as "originalFile",
          PHTO_HST.EDT_FILE_PATH as "editFile",
          PHTO_HST.REG_DT as "regDate",
          PHTO_HST.REG_ID as "regId",
          PHTO_HST.MOD_DT as "modDate",
          PHTO_HST.MOD_ID as "modId"
        FROM PHTO_HST
        WHERE PHTO_HST.PHTO_NO = '${photoNum}'
      `
      ))[0];

      const labels = await this.query(
        `SELECT 
            LBL_HST.TRTMT_YMD as "treatmentDate",
            LBL_HST.HSPTL_ID as "hospitalId",
            LBL_HST.PHOT_NO as "photoNum",
            LBL_HST.LBL_NO as "labelNum",
            LBL_HST.LBL_DESC as "description",
            LBL_HST.CRP_FILE_PATH as "cropFile",
            LBL_HST.CRP_POS_X as "x",
            LBL_HST.CRP_POS_Y as "y",
            LBL_HST.CRP_H as "height",
            LBL_HST.CRP_W as "width",
            LBL_HST.REG_DT as "regDate",
            LBL_HST.REG_ID as "regId",
            LBL_HST.MOD_DT as "modDate",
            LBL_HST.MOD_ID as "modId"
          FROM PHTO_HST
            JOIN LBL_HST
            ON PHTO_HST.PHTO_NO = LBL_HST.PHOT_NO
          WHERE PHTO_HST.PHTO_NO = ${photo.photoNum}`
      );
      photo.labels = labels;

      return photo;
    } catch (error) {
      console.error(error);
      return {};
    }
  }

  async getListWithLabels(
    treatmentDate: string,
    patientNum: string,
    hospitalId: string
  ) {
    const photos = await this.query(
      `SELECT
        TO_CHAR(TO_DATE(PHTO_HST.TRTMT_YMD, 'YYYYMMDD'), 'YYYY-MM-DD') as "treatmentDate",
        PHTO_HST.HSPTL_ID as "hospitalId",
        PHTO_HST.PHTO_NO as "photoNum",
        PHTO_HST.PTNT_NO as "patientNum",
        TO_CHAR(TO_DATE(PHTO_HST.SHT_DT, 'YYYYMMDDHH12MISS'), 'YYYY-MM-DD HH12:MI:SS') as "shootingDate",
        PHTO_HST.SHT_EQPMT_CD as "shootingEquipment",
        PHTO_HST.BDY_PRT_ID as "bodyPartId",
        FN_GET_BDY_PRT_NM(PHTO_HST.BDY_PRT_ID) as "bodyPartName",
        PHTO_HST.OPRTN_PHOT_YN as "isOperation",
        PHTO_HST.BPSY_PHOT_YN as "isBiopsy",
        PHTO_HST.LSR_PHOT_YN as "isLaser",
        PHTO_HST.TRTMT_PRGRS_CD as "treatmentProgress",
        FN_GET_CD_NM('TRTMT_PRGRS_CD', PHTO_HST.TRTMT_PRGRS_CD) as "treatmentProgressName",
        PHTO_HST.ORG_FILE_PATH as "originalFile",
        PHTO_HST.EDT_FILE_PATH as "editFile",
        PHTO_HST.REG_DT as "regDate",
        PHTO_HST.REG_ID as "regId",
        PHTO_HST.MOD_DT as "modDate",
        PHTO_HST.MOD_ID as "modId",
        PHTO_HST.ORG_FILE_NM as "filename"
      FROM PHTO_HST
      WHERE PHTO_HST.TRTMT_YMD = '${treatmentDate}'
        AND PHTO_HST.PTNT_NO = '${patientNum}'
        AND PHTO_HST.HSPTL_ID = '${hospitalId}'`
    );

    for (let index = 0; index < photos.length; index++) {
      const photo = photos[index];
      const labels = await this.query(
        `SELECT 
          LBL_HST.TRTMT_YMD as "treatmentDate",
          LBL_HST.HSPTL_ID as "hospitalId",
          LBL_HST.PHOT_NO as "photoNum",
          LBL_HST.LBL_NO as "labelNum",
          LBL_HST.LBL_DESC as "description",
          LBL_HST.CRP_FILE_PATH as "cropFile",
          LBL_HST.CRP_POS_X as "posX",
          LBL_HST.CRP_POS_Y as "posY",
          LBL_HST.CRP_H as "height",
          LBL_HST.CRP_W as "width",
          LBL_HST.REG_DT as "regDate",
          LBL_HST.REG_ID as "regId",
          LBL_HST.MOD_DT as "modDate",
          LBL_HST.MOD_ID as "modId"
        FROM PHTO_HST
          JOIN LBL_HST
          ON PHTO_HST.PHTO_NO = LBL_HST.PHOT_NO
        WHERE PHTO_HST.PHTO_NO = ${photo.photoNum}`
      );
      photo.labels = labels;
    }
    return photos;
  }
}
