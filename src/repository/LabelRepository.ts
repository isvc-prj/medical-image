import { EntityRepository, Repository } from 'typeorm';
import { Label } from '../entity/Label';

@EntityRepository(Label)
export class LabelRepository extends Repository<Label> {
  async search(
    startAt: string,
    endAt: string,
    hospitalId: string,
    limit: number = 10,
    page: number = 1,
    bodyPartId: string,
    description: string
  ): Promise<any> {
    const sql = `
      SELECT
        TO_CHAR(TO_DATE(LBL_HST.TRTMT_YMD, 'YYYYMMDD'), 'YYYY-MM-DD') AS "treatmentDate",
        LBL_HST.HSPTL_ID AS "hospitalId",
        LBL_HST.PHOT_NO AS "photoNum",
        LBL_NO AS "labelNum",
        LBL_DESC AS "description",
        CRP_FILE_PATH AS "cropFile",
        CRP_POS_X AS "x",
        CRP_POS_Y AS "y",
        CRP_H AS "height",
        CRP_W AS "width"
      FROM LBL_HST, PHTO_HST
      WHERE TO_DATE(LBL_HST.TRTMT_YMD, 'YYYYMMDD') >= TO_DATE('${startAt}','YYYY-MM-DD')
        AND TO_DATE(LBL_HST.TRTMT_YMD, 'YYYYMMDD') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
        AND LBL_HST.HSPTL_ID = '${hospitalId}'
        AND LBL_HST.PHOT_NO = PHTO_HST.PHTO_NO
        ${bodyPartId ? `AND PHTO_HST.BDY_PRT_ID=${bodyPartId}` : ''}
        ${description ? `AND LBL_DESC LIKE '%${description}%'` : ''}
    `;
    try {
      const labels = await this.query(sql);
      const count = await this.getCount(
        startAt,
        endAt,
        hospitalId,
        bodyPartId,
        description
      );

      return { labels, count };
    } catch (error) {
      console.error(error);
      return { labels: [], count: 0 };
    }
  }

  async getCount(
    startAt: string,
    endAt: string,
    hospitalId: string,
    bodyPartId: string,
    description: string
  ) {
    try {
      return (await this.query(
        `SELECT
          COUNT(LBL_NO) AS "cnt"
        FROM LBL_HST, PHTO_HST
        WHERE TO_DATE(LBL_HST.TRTMT_YMD, 'YYYYMMDD') <= TO_DATE('${endAt} 23:59:59','YYYY-MM-DD HH24:MI:SS')
          AND TO_DATE(LBL_HST.TRTMT_YMD, 'YYYYMMDD') >= TO_DATE('${startAt}','YYYY-MM-DD')
          AND LBL_HST.HSPTL_ID = '${hospitalId}'
          AND LBL_HST.PHOT_NO = PHTO_HST.PHTO_NO
          ${bodyPartId ? `AND PHTO_HST.BDY_PRT_ID='${bodyPartId}'` : ''}
          ${description ? `AND LBL_DESC LIKE '%${description}%'` : ''}
          `
      ))[0]['cnt'];
    } catch (error) {
      console.error(error);
      return 0;
    }
  }
}
