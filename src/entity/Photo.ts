import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import Data from './Data';

@Entity('PHTO_HST')
export class Photo extends Data {
  @PrimaryGeneratedColumn({ type: 'int', name: 'PHTO_NO' })
  photoNum: number;

  @PrimaryColumn({
    type: 'varchar2',
    length: 20,
    name: 'HSPTL_ID',
    nullable: false
  })
  hospitalId: string;

  @PrimaryColumn({
    type: 'varchar2',
    length: 8,
    name: 'TRTMT_YMD',
    nullable: false
  })
  treatmentDate: string;

  @Column({ type: 'varchar2', length: 20, name: 'PTNT_NO' })
  patientNum: string;

  @Column({ type: 'varchar2', length: 14, name: 'SHT_DT' })
  shootingDate: string;

  @Column({ type: 'varchar2', length: 20, name: 'SHT_EQPMT_CD' })
  shootingEquipment: string;

  @Column({ type: 'int', name: 'BDY_PRT_ID' })
  bodyPartId: number;

  @Column({ type: 'varchar2', length: 1, name: 'OPRTN_PHOT_YN' })
  isOperation: string;

  @Column({ type: 'varchar2', length: 1, name: 'BPSY_PHOT_YN' })
  isBiopsy: string;

  @Column({ type: 'varchar2', length: 1, name: 'LSR_PHOT_YN' })
  isLaser: string;

  @Column({ type: 'varchar2', length: 20, name: 'TRTMT_PRGRS_CD' })
  treatmentProgress: string;

  @Column({ type: 'varchar2', length: 200, name: 'ORG_FILE_PATH' })
  originalFile: string;

  @Column({ type: 'varchar2', length: 200, name: 'EDT_FILE_PATH' })
  editFile: string;

  @Column({type: "varchar2", length: 100, name:'ORG_FILE_NM'})
  filename: string;
}
