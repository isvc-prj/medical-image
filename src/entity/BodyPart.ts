import { Column, PrimaryColumn, Entity } from 'typeorm';
import Data from './Data';

@Entity({ name: 'BDY_PRT_INFO' })
export class BodyPart extends Data {
  @PrimaryColumn({ type: 'int', name: 'BDY_PRT_ID' })
  id: number;

  @Column({ type: 'varchar2', length: 20, name: 'BDY_PRT_NM' })
  name: string;

  @Column({ type: 'int', name: 'BDY_PRT_LVL' })
  level: number;

  @Column({ type: 'int', name: 'PRNT_BDY_PRT_ID' })
  parentId: number;

  @Column({ type: 'int', name: 'SORT_SEQ' })
  sortSequence: number;
}
