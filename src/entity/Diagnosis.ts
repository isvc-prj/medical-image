import { Entity, Column, PrimaryColumn } from 'typeorm';
import Data from './Data';

@Entity({ name: 'DGNS_INFO' })
export class Diagnosis extends Data {
  @PrimaryColumn({ name: 'DGNS_ID' })
  id: number;

  @Column({ type: 'varchar2', length: 100, name: 'DGNS_NM' })
  name: string;

  @Column({ type: 'varchar2', length: 100, name: 'DGNS_NM_ENG' })
  nameEng: string;

  @Column({ type: 'varchar2', length: 20, name: 'ICD10_CD' })
  ICD10: string;

  @Column({ type: 'int', name: 'CLNC_CLSF_CD' })
  clinicalCode: number;
}
