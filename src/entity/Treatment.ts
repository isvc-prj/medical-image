import { Column, PrimaryColumn, Entity } from 'typeorm';
import Data from './Data';

@Entity({ name: 'TRTMT_MST' })
export class Treatment extends Data {
  @PrimaryColumn({ type: 'varchar2', name: 'TRTMT_YMD', length: 8 })
  treatmentDate: string;

  @PrimaryColumn({ type: 'varchar2', length: 20, name: 'PTNT_NO' })
  patientNum: string;

  @PrimaryColumn({ type: 'varchar2', length: 20, name: 'HSPTL_ID' })
  hospitalId: string;

  @Column({ type: 'varchar2', length: 20, name: 'GNDR_CD' })
  gender: string;

  @Column({ type: 'varchar2', length: 10, name: 'PTNT_AGE' })
  age: string;

  @Column({ type: 'int', name: 'DGNS_ID' })
  diagnosisId: number;

  @Column({ type: 'int', name: 'PHTO_REG_CNT' })
  photoCount: number;

  @Column({ type: 'varchar2', length: 200, name: 'EXCLD_DGNS_NM' })
  exclusionDiagnosisName: string;

  @Column({ type: 'varchar2', length: 100, name: 'INTRST_RGN' })
  interestRegion: string;

  @Column({ type: 'varchar2', length: 1000, name: 'RMK' })
  rmk: string;

  @Column({ type: 'varchar2', length: 1, name: 'CHIF_CMPLNT_CD' })
  chiefComplain: string;

  @Column({ type: 'varchar2', length: 1, name: 'CNFRM_YN' })
  confirmed: string;
}
