import { Entity, Column, PrimaryColumn } from 'typeorm';
import Data from './Data';

@Entity({ name: 'MENU_INFO' })
export class Menu extends Data {
  @PrimaryColumn({ type: 'varchar2', length: 20, name: 'MENU_ID' })
  id: string;

  @Column({ type: 'varchar2', length: 50, name: 'MENU_NM' })
  name: string;

  @Column({ type: 'int', name: 'MENU_LVL' })
  level: number;

  @Column({ type: 'varchar2', name: 'PRNT_MENU_ID' })
  parentMenuId: string;

  @Column({ type: 'int', name: 'SORT_SEQ' })
  sortSequence: number;

  @Column({ type: 'varchar2', name: 'MENU_URL', length: 100 })
  url: string;
}
