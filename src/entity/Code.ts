import { Entity, Column, PrimaryColumn } from 'typeorm';
import Data from './Data';

@Entity({ name: 'COMM_CD_DTL' })
export class Code extends Data {
  @PrimaryColumn({ type: 'varchar2', name: 'GRP_CD', length: 20 })
  group: string;

  @PrimaryColumn({ type: 'varchar2', name: 'COMM_CD', length: 20 })
  commonCode: string;

  @Column({ type: 'varchar2', name: 'COMM_CD_NM', length: 200 })
  commonCodeName: string;

  @Column({ type: 'int', name: 'SORT_SEQ' })
  sortSequence: number;
}
