import { Entity, PrimaryColumn, Column } from 'typeorm';
import Data from './Data';

@Entity({ name: 'LBL_HST' })
export class Label extends Data {
  @PrimaryColumn({ type: 'varchar2', length: 8, name: 'TRTMT_YMD' })
  treatmentDate: string;

  @PrimaryColumn({ type: 'varchar2', length: 20, name: 'HSPTL_ID' })
  hospitalId: string;

  @PrimaryColumn({ type: 'int', name: 'PHOT_NO' })
  photoNum: number;

  @PrimaryColumn({ type: 'int', name: 'LBL_NO' })
  labelNum: number;

  @Column({ type: 'varchar2', name: 'LBL_DESC', length: 100 })
  description: string;

  @Column({ type: 'varchar2', name: 'CRP_FILE_PATH', length: 200 })
  cropFile: string;

  @Column({ type: 'number', name: 'CRP_POS_X' })
  posX: number;

  @Column({ type: 'number', name: 'CRP_POS_Y' })
  posY: number;

  @Column({ type: 'number', name: 'CRP_H' })
  height: number;

  @Column({ type: 'number', name: 'CRP_W' })
  width: number;
}
