import { Entity, Column, PrimaryColumn } from 'typeorm';
import Data from './Data';

@Entity({ name: 'USR_INFO' })
export class User extends Data {
  @PrimaryColumn({
    type: 'varchar2',
    length: 50,
    name: 'USR_ID',
    nullable: false
  })
  id: string;

  @Column({ type: 'varchar2', length: 100, name: 'USR_NM' })
  name: string;

  @Column({ type: 'varchar2', length: 256, name: 'USR_PW' })
  password: string;

  @Column({ type: 'varchar2', length: 20, name: 'AUTH_ID' })
  authId: string;
}
