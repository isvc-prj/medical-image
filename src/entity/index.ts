import { BodyPart } from './BodyPart';
import { Code } from './Code';
import { Diagnosis } from './Diagnosis';
import { Label } from './Label';
import { Menu } from './Menu';
import { Photo } from './Photo';
import { Treatment } from './Treatment';
import { User } from './User';

export default [BodyPart, Code, Diagnosis, Label, Menu, Photo, Treatment, User];
