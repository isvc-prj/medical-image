import { Column } from 'typeorm';

export default abstract class Data {
  @Column({ type: 'varchar2', length: 14, name: 'REG_DT' })
  regDate: string;

  @Column({ type: 'varchar2', length: 20, name: 'REG_ID' })
  regId: string;

  @Column({ type: 'varchar2', length: 14, name: 'MOD_DT' })
  modDate: string;

  @Column({ type: 'varchar2', length: 20, name: 'MOD_ID' })
  modId: string;
}
