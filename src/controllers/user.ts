import { Request, Response } from 'express';

export const getLogin = (req: Request, res: Response) => {
  res.render('login');
};

export const logout = (req: Request, res: Response) => {
  if (req.isAuthenticated()) {
    req.logout();
    res.status(200).end();
  } else {
    res.status(403).end();
  }
};
