import { Request, Response } from 'express';
import { getRepository, getManager } from 'typeorm';
import { Treatment } from '../entity/Treatment';
import { Photo } from '../entity/Photo';
import * as moment from 'moment-timezone';
import * as multiparty from 'multiparty';
import * as xlsx from 'xlsx';
import { getPatientNum, getHospitalId, removeDateSep } from '../utils';
import { Diagnosis } from '../entity/Diagnosis';
import { Label } from '../entity/Label';

export const postTreatment = async (req: Request, res: Response) => {
  const {
    body: {
      patientNum,
      gender,
      age,
      unit,
      diagnosisId,
      exclusionDiagnosisName,
      interestRegion,
      photoCount,
      chiefComplain,
      confirmed,
      imageInfo,
      labelingInfo
    },
    files
  } = req;
  try {
    const treatmentDate = moment(
      JSON.parse(imageInfo[0]).shootingDate,
      'YYYY-MM-DD hh:mm:ss'
    );

    const hospitalId = JSON.parse(imageInfo[0]).hospitalId;

    let _treatment = await getRepository(Treatment).findOne({
      patientNum,
      hospitalId,
      treatmentDate: treatmentDate.format('YYYYMMDD')
    });

    if (_treatment) {
      res.send({
        treatment: {
          patientNum,
          hospitalId,
          treatmentDate: treatmentDate.format('YYYYMMDD')
        },
        exist: true
      });
    } else {
      const treatment = await getRepository(Treatment).insert({
        patientNum,
        gender,
        age: age + (unit === 'month' ? 'M' : ''),
        hospitalId,
        diagnosisId,
        exclusionDiagnosisName,
        interestRegion,
        photoCount,
        regId: req.user.id,
        treatmentDate: treatmentDate.format('YYYYMMDD'),
        chiefComplain,
        confirmed: confirmed === 'on' ? 'Y' : 'N'
      });

      const photo = imageInfo.map((_info: any, index: number) => {
        let info = JSON.parse(_info);
        info.shootingDate = removeDateSep(info.shootingDate);
        info.isOperation = info.isOperation ? 'Y' : 'N';
        info.isBiopsy = info.isBiopsy ? 'Y' : 'N';
        info.isLaser = info.isLaser ? 'Y' : 'N';
        info.originalFile = files['images[]'][index].location;
        info.editFile = files['editImages[]'][index].location;
        info.treatmentDate = treatmentDate.format('YYYYMMDD');
        info.regId = req.user.id;

        return info;
      });

      const photoInfo = imageInfo.map(_info => JSON.parse(_info));

      const createdPhoto = await getRepository(Photo).insert(photo);
      let labels;

      if (labelingInfo) {
        labels = labelingInfo.map((_info: any, index: number) => {
          let info = JSON.parse(_info);
          info.treatmentDate = moment(info.treatmentDate, 'YYYY-MM-DD').format(
            'YYYYMMDD'
          );
          info.posX = Number(info.posX);
          info.posY = Number(info.posY);
          info.height = Number(info.height);
          info.cropFile = files['labelingImages[]'][index].location;
          info.width = Number(info.width);
          info.regId = req.user.id;
          info.photoNum =
            photo[
              photoInfo.findIndex(pinfo => pinfo._id === info.parent)
            ].photoNum;

          return info;
        });
        const createdLabel = await getRepository(Label).insert(labels);
        res.send({ treatment, createdPhoto, createdLabel });
      } else {
        res.send({ treatment, createdPhoto });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
};

export const postDir = async (req: Request, res: Response) => {
  const {
    body: { imageInfo },
    files
  } = req;

  try {
    const photo = imageInfo.map((_info: any, index: number) => {
      let info = JSON.parse(_info);
      info.shootingDate = moment(
        info.DateTimeOriginal,
        'YYYY-MM-DD hh:mm:ss'
      ).format('YYYYMMDDhhmmss');
      info.shootingEquipment = info.Make;
      info.isOperation = info.isOperation ? 'Y' : 'N';
      info.isBiopsy = info.isBiopsy ? 'Y' : 'N';
      info.isLaser = info.isLaser ? 'Y' : 'N';
      info.originalFile = files[index].location;
      info.treatmentDate = moment(
        info.DateTimeOriginal,
        'YYYY-MM-DD hh:mm:ss'
      ).format('YYYYMMDD');
      info.regId = req.user.id;
      info.treatmentDate = moment(
        info.DateTimeOriginal,
        'YYYY-MM-DD hh:mm:ss'
      ).format('YYYYMMDD');

      return info;
    });

    for (let index = 0; index < photo.length; index++) {
      const treatment = await getRepository(Treatment).findOne({
        hospitalId: photo[index].hospitalId,
        treatmentDate: photo[index].treatmentDate,
        patientNum: photo[index].patientNum
      });

      if (!treatment) {
        await getRepository(Treatment).insert({
          hospitalId: photo[index].hospitalId,
          treatmentDate: photo[index].treatmentDate,
          patientNum: photo[index].patientNum
        });
      }

      const p = await getRepository(Photo).findOne({
        filename: photo[index].filename
      });
      if (!p) {
        await getRepository(Photo).insert(photo[index]);
      }
    }
    await getManager().query(
      `UPDATE TRTMT_MST 
        SET PHTO_REG_CNT=(
          SELECT
            COUNT(PHTO_NO)
          FROM PHTO_HST
          WHERE
            TRTMT_MST.PTNT_NO = PHTO_HST.PTNT_NO
            AND TRTMT_MST.HSPTL_ID = PHTO_HST.HSPTL_ID
            AND TRTMT_MST.TRTMT_YMD = PHTO_HST.TRTMT_YMD
        )`
    );

    res.status(200).end();
  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
};

export const postXlsx = async (req: Request, res: Response) => {
  const form = new multiparty.Form({ autoFiles: true });
  const xlsxData = [];

  form.on('file', (name, file) => {
    const xlsxFile = xlsx.readFile(file.path, {
      cellDates: true
    });
    for (const sheetname in xlsxFile.Sheets) {
      xlsxData.push(xlsx.utils.sheet_to_json(xlsxFile.Sheets[sheetname]));
    }
  });

  form.on('close', async () => {
    try {
      for (let index = 0; index < xlsxData[0].length; index++) {
        const data = xlsxData[0][index];

        const treatment = await getRepository(Treatment).findOne({
          patientNum: getPatientNum(data['일련번호']),
          hospitalId: getHospitalId(data['일련번호']),
          treatmentDate: moment(data['날짜'])
            .add(1, 'minutes')
            .format('YYYYMMDD')
        });

        if (!treatment) {
          let insertData;
          const diagnosis = await getRepository(Diagnosis).findOne({
            name: data['임상진단'].trim()
          });
          if (diagnosis)
            insertData = {
              patientNum: getPatientNum(data['일련번호']),
              hospitalId: getHospitalId(data['일련번호']),
              gender:
                data['성별'] === '남' ? '1' : data['성별'] === '여' ? '2' : '3',
              age: data['나이'],
              treatmentDate: moment(data['날짜'])
                .add(1, 'minutes')
                .format('YYYYMMDD'),
              diagnosisId: diagnosis.id,
              photoCount: 0
            };
          else
            insertData = {
              patientNum: getPatientNum(data['일련번호']),
              hospitalId: getHospitalId(data['일련번호']),
              gender:
                data['성별'] === '남' ? '1' : data['성별'] === '여' ? '2' : '3',
              age: data['나이'],
              treatmentDate: moment(data['날짜'])
                .add(1, 'minutes')
                .format('YYYYMMDD'),
              photoCount: 0,
              rmk: data['임상진단'].trim()
            };
          await getRepository(Treatment).insert(insertData);
        } else {
          let updateData;
          const diagnosis = await getRepository(Diagnosis).findOne({
            name: data['임상진단'].trim()
          });
          if (diagnosis)
            updateData = {
              gender:
                data['성별'] === '남' ? '1' : data['성별'] === '여' ? '2' : '3',
              age: data['나이'],
              diagnosisId: diagnosis.id,
              photoCount: 0
            };
          else
            updateData = {
              gender:
                data['성별'] === '남' ? '1' : data['성별'] === '여' ? '2' : '3',
              age: data['나이'],
              photoCount: 0,
              rmk: data['임상진단'].trim()
            };
          await getRepository(Treatment).update(
            {
              patientNum: getPatientNum(data['일련번호']),
              hospitalId: getHospitalId(data['일련번호']),
              treatmentDate: moment(data['날짜'])
                .add(1, 'minutes')
                .format('YYYYMMDD')
            },
            updateData
          );
        }
      }

      await getManager().query(
        'UPDATE TRTMT_MST SET PHTO_REG_CNT=(SELECT COUNT(PHTO_NO) FROM PHTO_HST WHERE TRTMT_MST.PTNT_NO = PHTO_HST.PTNT_NO AND TRTMT_MST.HSPTL_ID = PHTO_HST.HSPTL_ID AND TRTMT_MST.TRTMT_YMD = PHTO_HST.TRTMT_YMD) WHERE PHTO_REG_CNT != (SELECT COUNT(PHTO_NO) FROM PHTO_HST WHERE TRTMT_MST.PTNT_NO = PHTO_HST.PTNT_NO AND TRTMT_MST.HSPTL_ID = PHTO_HST.HSPTL_ID AND TRTMT_MST.TRTMT_YMD = PHTO_HST.TRTMT_YMD)'
      );
      res.status(200).end();
    } catch (err) {
      console.log(err);
      res.status(500).end();
    }
  });

  form.parse(req);
};

export const editTreatment = async (req: Request, res: Response) => {
  const {
    body: {
      patientNum,
      gender,
      age,
      unit,
      diagnosisId,
      exclusionDiagnosisName,
      interestRegion,
      photoCount,
      chiefComplain,
      confirmed,
      imageInfo,
      labelingInfo,
      treatmentDate,
      hospitalId
    },
    files
  } = req;

  try {
    const treatment = await getRepository(Treatment).update(
      {
        patientNum,
        treatmentDate: removeDateSep(treatmentDate),
        hospitalId
      },
      {
        patientNum,
        gender,
        age: age + (unit === 'month' ? 'M' : ''),
        hospitalId,
        diagnosisId,
        exclusionDiagnosisName,
        interestRegion,
        photoCount,
        regId: req.user.id,
        chiefComplain,
        confirmed: confirmed === 'on' ? 'Y' : 'N'
      }
    );

    let photo = [],
      photoInfo,
      count = 0;

    if (imageInfo) {
      imageInfo.forEach(async (_info: any) => {
        let info = JSON.parse(_info);

        if (info.photoNum && info.edit === 'true') {
          const result = await getRepository(Photo).update(
            { photoNum: info.photoNum },
            {
              editFile: files['editImages[]'][count++].location,
              isOperation: info.isOperation ? 'Y' : 'N',
              isBiopsy: info.isBiopsy ? 'Y' : 'N',
              isLaser: info.isLaser ? 'Y' : 'N',
              modDate: moment().format('YYYYMMDDhhmmss'),
              bodyPartId: info.bodyPartId,
              treatmentProgress: info.treatmentProgress
            }
          );
        } else if (info.photoNum) {
          await getRepository(Photo).update(
            { photoNum: info.photoNum },
            {
              isOperation: info.isOperation ? 'Y' : 'N',
              isBiopsy: info.isBiopsy ? 'Y' : 'N',
              isLaser: info.isLaser ? 'Y' : 'N',
              modDate: moment().format('YYYYMMDDhhmmss'),
              bodyPartId: info.bodyPartId,
              treatmentProgress: info.treatmentProgress
            }
          );
          console.log(info.treatmentProgress);
        }
      });
    }

    if (labelingInfo) {
      photoInfo = imageInfo.map(_info => JSON.parse(_info));

      labelingInfo.forEach(async (_info: any, index: number) => {
        let info = JSON.parse(_info);

        if (!info.no) {
          await getRepository(Label).insert({
            treatmentDate: moment(info.treatmentDate, 'YYYY-MM-DD').format(
              'YYYYMMDD'
            ),
            hospitalId: info.hospitalId,
            posX: Number(info.posX),
            posY: Number(info.posY),
            height: Number(info.height),
            width: Number(info.width),
            cropFile: info.cropFile = files['labelingImages[]'][index].location,
            regId: req.user.id,
            photoNum: info.photoNum
              ? info.photoNum
              : photo[photoInfo.findIndex(pinfo => pinfo._id === info.parent)]
                  .photoNum,
            description: info.description
          });
        } else {
          const label = await getRepository(Label).findOne({
            labelNum: Number(info.no)
          });
          if (label) {
            await getRepository(Label).update(
              {
                labelNum: Number(info.no)
              },
              {
                hospitalId: info.hospitalId,
                posX: Number(info.posX),
                posY: Number(info.posY),
                height: Number(info.height),
                width: Number(info.width),
                cropFile: info.cropFile =
                  files['labelingImages[]'][index].location,
                regId: req.user.id,
                photoNum: info.photoNum,
                description: info.description
              }
            );
          } else {
            await getRepository(Label).insert({
              treatmentDate: moment(info.treatmentDate, 'YYYY-MM-DD').format(
                'YYYYMMDD'
              ),
              hospitalId: info.hospitalId,
              posX: Number(info.posX),
              posY: Number(info.posY),
              height: Number(info.height),
              width: Number(info.width),
              cropFile: info.cropFile =
                files['labelingImages[]'][index].location,
              regId: req.user.id,
              photoNum: info.photoNum,
              description: info.description
            });
          }
        }
      });
    }
    res.send({ treatment });
  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
};
