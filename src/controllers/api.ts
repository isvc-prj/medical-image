import { Request, Response } from 'express';
import { getRepository, getCustomRepository } from 'typeorm';
import { Photo } from '../entity/Photo';
import { Diagnosis } from '../entity/Diagnosis';
import { Treatment } from '../entity/Treatment';
import { removeDateSep } from '../utils';
import { PhotoRepository } from '../repository/PhotoRepository';
import { Label } from '../entity/Label';

export const getPhotos = async (req: Request, res: Response) => {
  const { treatmentDate, patientNum, hospitalId, filename } = req.query;

  if (filename) {
    const photo = await getRepository(Photo).findOne({
      filename
    });
    res.send(photo);
  } else if (treatmentDate && patientNum && hospitalId) {
    const photos = await getRepository(Photo).find({
      treatmentDate: removeDateSep(treatmentDate),
      patientNum,
      hospitalId
    });
    res.send(photos);
  }
};

export const getName = async (req: Request, res: Response) => {
  const { keyword } = req.query;

  const diagnosis = await getRepository(Diagnosis)
    .createQueryBuilder()
    .where(`UPPER(DGNS_NM) LIKE UPPER('${keyword}%')`)
    .getMany();

  res.send(diagnosis);
};

export const getCode = async (req: Request, res: Response) => {
  const { keyword } = req.query;

  const diagnosis = await getRepository(Diagnosis)
    .createQueryBuilder()
    .where(`UPPER(ICD10_CD) LIKE UPPER('${keyword}%')`)
    .getMany();

  res.send(diagnosis);
};

export const deleteTreatment = async (req: Request, res: Response) => {
  const { treatmentDate, patientNum, hospitalId } = req.query;

  const result = await getRepository(Treatment).delete({
    treatmentDate: removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  });

  const photos = await getRepository(Photo).find({
    treatmentDate: removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  });

  for (let index = 0; index < photos.length; index++) {
    const photo = photos[index];
    await getRepository(Label).delete({
      photoNum: photo.photoNum
    });
  }

  await getRepository(Photo).delete({
    treatmentDate: removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  });

  res.send(result);
};

export const getPhoto = async (req: Request, res: Response) => {
  const { no } = req.params;
  const photo = await getCustomRepository(PhotoRepository).getOneWithLabels(no);
  res.send(photo);
};

export const postLabel = async (req: Request, res: Response) => {};
