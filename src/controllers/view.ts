import { Request, Response } from 'express';
import { getRepository, getCustomRepository } from 'typeorm';
import { BodyPart } from '../entity/BodyPart';
import {
  getPagination,
  getTodayDate,
  getDiagnosisInfo,
  removeDateSep
} from '../utils';
import { TreatmentRepository } from '../repository/TreatmentRepository';
import { PhotoRepository } from '../repository/PhotoRepository';
import { Diagnosis } from '../entity/Diagnosis';
import { LabelRepository } from '../repository/LabelRepository';
import { Code } from '../entity/Code';

export const index = (req: Request, res: Response) => {
  res.render('index');
};

export const getStatus = async (req: Request, res: Response) => {
  let { startAt, endAt, page, diagnosisId, hospitalId } = req.query;

  startAt = startAt || getTodayDate();
  endAt = endAt || getTodayDate();
  page = page || 1;
  hospitalId = hospitalId || 'BRMH';

  const { name, ICD10 } = await getDiagnosisInfo(diagnosisId);

  const { list, count } = await getCustomRepository(
    TreatmentRepository
  ).getList(startAt, endAt, hospitalId, diagnosisId, page);

  const pagination = getPagination(page, count);

  res.render('status', {
    list,
    count,
    pagination,
    startAt,
    endAt,
    diagnosisId,
    diagnosisName: name,
    ICD10
  });
};

export const getEditor = async (req: Request, res: Response) => {
  const { treatmentDate, patientNum, hospitalId } = req.query;

  const bodyparts = await getRepository(BodyPart).find({});
  const codes = await getRepository(Diagnosis).find({});

  const photos = await getCustomRepository(PhotoRepository).getListWithLabels(
    removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  );

  const treatment = await getCustomRepository(TreatmentRepository).getOne(
    removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  );

  const chiefComplains = await await getRepository(Code).find({
    group: 'CHIF_CMPLNT_CD'
  });

  res.render('editor', { bodyparts, codes, chiefComplains, treatment, photos });
};

export const getStatusDetail = async (req: Request, res: Response) => {
  const { treatmentDate, patientNum, hospitalId } = req.query;

  const treatment = await getCustomRepository(TreatmentRepository).getOne(
    removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  );

  const photos = await getCustomRepository(PhotoRepository).getListWithLabels(
    removeDateSep(treatmentDate),
    patientNum,
    hospitalId
  );

  res.render('status-detail', { treatment, photos });
};

export const getInfo = async (req: Request, res: Response) => {
  let {
    startAt,
    endAt,
    page,
    hospitalId,
    diagnosisId,
    description
  } = req.query;

  startAt = startAt || getTodayDate();
  endAt = endAt || getTodayDate();
  page = page || 1;
  hospitalId = hospitalId || 'BRMH';

  const { name, ICD10 } = await getDiagnosisInfo(diagnosisId);

  const { photos, count } = await getCustomRepository(PhotoRepository).getList(
    startAt,
    endAt,
    hospitalId,
    10,
    page,
    diagnosisId,
    description
  );

  const pagination = getPagination(page, count);

  res.render('info', {
    photos,
    pagination,
    startAt,
    endAt,
    count,
    diagnosisId,
    description,
    diagnosisName: name,
    ICD10
  });
};

export const getData = async (req: Request, res: Response) => {
  let { startAt, endAt, page, hospitalId, bodyPartId, description } = req.query;

  startAt = startAt || getTodayDate();
  endAt = endAt || getTodayDate();
  page = page || 1;
  hospitalId = hospitalId || 'BRMH';

  const bodyparts = await getRepository(BodyPart).find({});
  const { labels, count } = await getCustomRepository(LabelRepository).search(
    startAt,
    endAt,
    hospitalId,
    10,
    page,
    bodyPartId,
    description
  );

  res.render('data', {
    labels,
    count,
    startAt,
    endAt,
    bodyparts,
    bodyPartId,
    description
  });
};

export const getOneRegistration = async (req: Request, res: Response) => {
  const bodyparts = await getRepository(BodyPart).find({});
  const codes = await getRepository(Diagnosis).find({});
  const chiefComplains = await await getRepository(Code).find({
    group: 'CHIF_CMPLNT_CD'
  });

  res.render('one-registration', { bodyparts, codes, chiefComplains });
};

export const getBatchRegistration = async (req: Request, res: Response) => {
  let { page, startAt, hospitalId } = req.query;
  startAt = startAt || getTodayDate();
  page = page || 1;
  hospitalId = hospitalId || 'BRMH';

  startAt = startAt.trim();

  const treatments = await getCustomRepository(
    TreatmentRepository
  ).getListWithPatient(startAt, startAt, hospitalId, 10, page);

  const photoCount = await getCustomRepository(PhotoRepository).getCount(
    startAt,
    startAt,
    hospitalId
  );

  const { treatmentCount, patientCount } = await getCustomRepository(
    TreatmentRepository
  ).getCountWithPatient(startAt, hospitalId);

  const pagination = getPagination(page, patientCount);
  res.render('batch-registration', {
    treatments,
    pagination,
    startAt,
    photoCount,
    treatmentCount,
    patientCount
  });
};
