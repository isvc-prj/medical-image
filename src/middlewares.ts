import { Request, Response, NextFunction } from 'express';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';
import * as path from 'path';

import s3 from './config/s3';
import { getTodayDate, getHospitalsOfUser, createQueryUrl } from './utils';

export const uploadS3 = multer({
  storage: multerS3({
    s3,
    bucket: 'sqiengbucket/medical-image',
    key: function(req, file, cb) {
      const extension = path.extname(file.originalname);
      const basename = path.basename(file.originalname, extension);
      cb(null, `${Date.now()}_${basename}${extension}`);
    },
    acl: 'public-read-write',
    contentType: multerS3.AUTO_CONTENT_TYPE
  })
});

export const localMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.isAuthenticated()) {
    res.locals.user = req.user;
    res.locals.hospitalsOfUser = await getHospitalsOfUser(req.user.id);
  }
  res.locals.url = createQueryUrl(req.path, req.query);
  res.locals.today = getTodayDate();
  next();
};

export const onlyPrivate = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  !req.isAuthenticated() && res.redirect('/login');
  next();
};
