import * as passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';

export default () => {
  passport.serializeUser((user: User, done: Function) => {
    done(null, user.id);
  });

  passport.deserializeUser(async (id: string, done: Function) => {
    const user = await getRepository(User).findOne(id);
    done(null, user);
  });

  passport.use(
    new LocalStrategy(
      { usernameField: 'id', passwordField: 'password', session: true },
      async (id: string, password: string, done: Function) => {
        const user = await getRepository(User).findOne({ id });
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        } else if (user.password !== password) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
      }
    )
  );
};
