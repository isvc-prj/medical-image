import * as viewController from './controllers/view';
import * as userController from './controllers/user';
import * as uploadController from './controllers/upload';
import * as apiController from './controllers/api';
import { onlyPrivate, uploadS3 } from './middlewares';
import * as passport from 'passport';

export const appRoutes = [
  {
    path: '/',
    method: 'get',
    handlers: [onlyPrivate, viewController.index]
  },
  {
    path: '/login',
    method: 'get',
    handlers: [userController.getLogin]
  },
  {
    path: '/data',
    method: 'get',
    handlers: [onlyPrivate, viewController.getData]
  },
  {
    path: '/status',
    method: 'get',
    handlers: [onlyPrivate, viewController.getStatus]
  },
  {
    path: '/status-detail',
    method: 'get',
    handlers: [onlyPrivate, viewController.getStatusDetail]
  },
  {
    path: '/info',
    method: 'get',
    handlers: [onlyPrivate, viewController.getInfo]
  },
  {
    path: '/one-registration',
    method: 'get',
    handlers: [onlyPrivate, viewController.getOneRegistration]
  },
  {
    path: '/batch-registration',
    method: 'get',
    handlers: [onlyPrivate, viewController.getBatchRegistration]
  },
  {
    path: '/editor',
    method: 'get',
    handlers: [onlyPrivate, viewController.getEditor]
  },
  {
    path: '/api/photos',
    method: 'get',
    handlers: [apiController.getPhotos]
  },
  {
    path: '/api/photo/:no',
    method: 'get',
    handlers: [apiController.getPhoto]
  },
  {
    path: '/api/name',
    method: 'get',
    handlers: [apiController.getName]
  },
  {
    path: '/api/code',
    method: 'get',
    handlers: [apiController.getCode]
  },
  {
    path: '/api/treatment',
    method: 'delete',
    handlers: [apiController.deleteTreatment]
  },
  {
    path: '/login',
    method: 'post',
    handlers: [
      passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    ]
  },
  {
    path: '/treatment',
    method: 'post',
    handlers: [
      uploadS3.fields([
        { name: 'images[]' },
        { name: 'labelingImages[]' },
        { name: 'editImages[]' }
      ]),
      uploadController.postTreatment
    ]
  },
  {
    path: '/dir',
    method: 'post',
    handlers: [uploadS3.array('images[]'), uploadController.postDir]
  },
  {
    path: '/xlsx',
    method: 'post',
    handlers: [uploadController.postXlsx]
  },
  {
    path: '/logout',
    method: 'delete',
    handlers: [userController.logout]
  },
  {
    path: '/edit',
    method: 'post',
    handlers: [
      uploadS3.fields([
        { name: 'images[]' },
        { name: 'labelingImages[]' },
        { name: 'editImages[]' }
      ]),
      uploadController.editTreatment
    ]
  }
];
