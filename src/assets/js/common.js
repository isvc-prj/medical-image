import axios from 'axios';
import 'jquery-ui/ui/widgets/datepicker';

(function() {
  if (document.querySelector('.logout')) {
    const logoutBtn = document.querySelector('.logout');
    logoutBtn.addEventListener('click', async () => {
      const response = await axios.delete('/logout');
      if (response.status === 200) location.href = '/login';
    });
  }

  if (document.querySelector('.menu-toggle')) {
    document.querySelector('.menu-toggle').addEventListener('click', () => {
      const menuList = document.querySelector('.menu-list');
      if (menuList.classList.contains('hidden')) {
        menuList.classList.remove('hidden');
      } else {
        menuList.classList.add('hidden');
      }
    });
  }

  if (document.querySelector('.menu-list')) {
    document.querySelectorAll('.menu-list li a').forEach(a => {
      if (
        location.pathname !== '/' &&
        location.pathname.indexOf(a.getAttribute('href')) !== -1
      ) {
        a.parentElement.classList.add('active');
      }
    });
  }

  if (document.querySelector('input[name=hospitalId]')) {
    document.querySelector(
      'input[name=hospitalId]'
    ).value = document.querySelector('select.hospital').value;
  }

  if (document.querySelector('.auto-complete.code')) {
    document
      .querySelector('.auto-complete.code')
      .addEventListener('keyup', async e => {
        const response = await axios.get(`/api/code?keyword=${e.target.value}`);

        const completeList = document.querySelector('.auto-complete-list.code');
        completeList.innerHTML = '';

        if (
          location.pathname.indexOf('/one-registration') === -1 &&
          location.pathname.indexOf('/editor') === -1
        ) {
          const li = document.createElement('li');
          li.innerText = '전체';
          li.dataset.name = '전체';
          li.addEventListener('click', e => {
            document.querySelector('.auto-complete.code').value =
              e.target.innerHTML;
            document.querySelector('.auto-complete.name').value =
              e.target.dataset.name;
            document.querySelector('input[name=diagnosisId]').value = '';
            completeList.classList.add('hidden');
          });
          completeList.appendChild(li);
        }

        response.data.forEach(data => {
          const li = document.createElement('li');
          li.innerText = data.ICD10;
          li.dataset.id = data.id;
          li.dataset.name = data.name;
          li.addEventListener('click', e => {
            document.querySelector('.auto-complete.code').value =
              e.target.innerHTML;
            document.querySelector('.auto-complete.name').value =
              e.target.dataset.name;
            document.querySelector('input[name=diagnosisId]').value =
              e.target.dataset.id;
            completeList.classList.add('hidden');
          });
          completeList.appendChild(li);
        });
        completeList.classList.remove('hidden');
      });
  }
  if (document.querySelector('.auto-complete.name')) {
    document
      .querySelector('.auto-complete.name')
      .addEventListener('keyup', async e => {
        const response = await axios.get(`/api/name?keyword=${e.target.value}`);

        const completeList = document.querySelector('.auto-complete-list.name');
        completeList.innerHTML = '';

        if (
          location.pathname.indexOf('/one-registration') === -1 &&
          location.pathname.indexOf('/editor') === -1
        ) {
          const li = document.createElement('li');
          li.innerText = '전체';
          li.dataset.name = '전체';
          li.addEventListener('click', e => {
            document.querySelector('.auto-complete.code').value =
              e.target.innerHTML;
            document.querySelector('.auto-complete.name').value =
              e.target.dataset.name;
            document.querySelector('input[name=diagnosisId]').value = '';
            completeList.classList.add('hidden');
          });
          completeList.appendChild(li);
        }

        response.data.forEach(data => {
          const li = document.createElement('li');
          li.innerText = data.name;
          li.dataset.id = data.id;
          li.dataset.ICD10 = data.ICD10;
          li.addEventListener('click', e => {
            document.querySelector('.auto-complete.name').value =
              e.target.innerHTML;
            document.querySelector('.auto-complete.code').value =
              e.target.dataset.ICD10;
            document.querySelector('input[name=diagnosisId]').value =
              e.target.dataset.id;
            completeList.classList.add('hidden');
          });
          completeList.appendChild(li);
        });
        completeList.classList.remove('hidden');
      });
  }

  if (document.querySelector('input[name=startAt]')) {
    $('input[name=startAt]').datepicker({ dateFormat: 'yy-mm-dd' });
  }
  if (document.querySelector('input[name=endAt]')) {
    $('input[name=endAt]').datepicker({ dateFormat: 'yy-mm-dd' });
  }

  if (document.querySelector('.input-age')) {
    document.querySelector('.input-age').addEventListener('keydown', e => {
      if (e.target.value.length > 2) {
        e.target.value = e.target.value.substring(0, 2);
      }
    });
  }
})();
