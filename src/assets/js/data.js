import axios from 'axios';
import moment from 'moment';

async function getOniginalPhoto(e) {
  if (document.querySelector('.label.active'))
    document.querySelector('.label.active').classList.remove('active');
  e.currentTarget.classList.add('active');
  const { photo } = e.currentTarget.dataset;

  const response = await axios.get(`/api/photo/${photo}`);

  setImageInfo(response.data);
}

function setImageInfo(data) {
  const imageBox = document.querySelector('.image-box');
  imageBox.querySelector('.image').src = data.editFile
    ? data.editFile
    : data.originalFile;
  imageBox.querySelector('.treatmentDate').innerHTML = moment(
    data.treatmentDate,
    'YYYYMMDD'
  ).format('YYYY-MM-DD');
  imageBox.querySelector('.patientNum').innerHTML = data.patientNum;
  imageBox.querySelector('.shootingEquipment').innerHTML =
    data.shootingEquipment;
  imageBox.querySelector('.bodyPart').innerHTML =
    data.bodyPartName === undefined ? '' : data.bodyPartName;
  imageBox.querySelector('.isOperation').innerHTML = data.isOperation;
  imageBox.querySelector('.isLaser').innerHTML = data.isLaser;
  imageBox.querySelector('.isBiopsy').innerHTML = data.isBiopsy;
  imageBox.querySelector('.treatmentProgress').innerHTML =
    data.treatmentProgressName;

  let html = '';
  data.labels.forEach(label => {
    html += `
      <img src=${label.cropFile}>
      <div>${label.description === null ? '' : label.description}</div>`;
  });
  imageBox.querySelector('.labeling-list-cell').innerHTML = html;
}

function checkAllCheckbox(e) {
  document.querySelectorAll('.label-checkbox').forEach(checkbox => {
    checkbox.checked = e.target.checked;
  });
  //   var link = document.createElement('a');
  // link.href = 'images.jpg';
  // link.download = 'Download.jpg';
  // document.body.appendChild(link);
  // link.click();
  // document.body.removeChild(link);
}

function downloadImage() {
  document.querySelectorAll('.label-checkbox:checked').forEach(checkbox => {
    const img = checkbox.parentElement.querySelector('img');
    let link = document.createElement('a');
    link.href = img.src;
    link.setAttribute('donwload', 'download');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  });
}

function init() {
  document.querySelectorAll('.label').forEach(label => {
    label.addEventListener('click', getOniginalPhoto);
  });

  document
    .querySelector('input[name=allcheck]')
    .addEventListener('change', checkAllCheckbox);

  document
    .querySelector('input.download')
    .addEventListener('click', downloadImage);

  if (document.querySelector('.label'))
    document.querySelector('.label').click();
}

if (location.pathname.indexOf('/data') !== -1) init();
