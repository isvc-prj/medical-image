import Labeling from './Labeling';
import FileParser from './FileParser';
import { toggleLoaderCircle, getOriginalname, isBase64 } from './utils';
import axios from 'axios';

const editor = {};

function init() {
  window.onbeforeunload = () => {
    return '수정 사항이 저장되지 않았습니다. 정말 나가시겠습니까?';
  };
  editor.patientImageList = document.querySelector('.patient-images');
  editor.patientImageCount = document.querySelectorAll(
    '.row:not(.example)'
  ).length;
  editor.labeling = new Labeling();
  editor.form = document.querySelector('.patient.info');

  const rows = document.querySelectorAll('.row:not(.example)');

  const deletePatientImageItem = e => {
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
  };

  const openLabelingPopup = e => {
    const row =
      e.target.parentElement.parentElement.parentElement.parentElement;
    const img = row.querySelector('img');
    const id = row.dataset.id;
    editor.labeling.openPopup(img, id);
  };

  const addPatientImageItems = async e => {
    if (e.target.files && e.target.files[0]) {
      const uploadFiles = e.target.files;
      for (let i = 0; i < uploadFiles.length; i++) {
        const patientImageItem = await createPatientImageItem(uploadFiles[i]);
        editor.patientImageList.appendChild(patientImageItem);
      }
    }
  };

  const uploadTreatment = async e => {
    e.preventDefault();
    window.onbeforeunload = undefined;
    const formData = new FormData(editor.form);
    const [originalImages, editImages] = getPatientImages();
    const info = getPatientImageInfo();
    const { labelingFiles, labelingInfo } = getLablingImages();

    originalImages.forEach(image => {
      formData.append('images[]', image);
    });
    info.forEach(info => {
      formData.append('imageInfo[]', JSON.stringify(info));
    });

    editImages.forEach(image => {
      formData.append('editImages[]', image);
    });

    labelingFiles.forEach(image => {
      formData.append('labelingImages[]', image);
    });
    labelingInfo.forEach(info => {
      formData.append('labelingInfo[]', JSON.stringify(info));
    });

    formData.append('photoCount', originalImages.length);

    toggleLoaderCircle();
    const response = await axios.post('/edit', formData, {
      headers: { 'content-type': 'multipart/form-data' },
      onUploadProgress: progressEvent => {
        if (progressEvent.lengthComputable) {
          console.log(`${(progressEvent.loaded / progressEvent.total) * 100}%`);
        }
      }
    });
    toggleLoaderCircle();

    if (response.status === 200) {
      location.href = `/status-detail${location.search}`;
    }
  };

  editor.form.addEventListener('submit', uploadTreatment);
  if (document.querySelector('.image-selector'))
    document
      .querySelector('.image-selector')
      .addEventListener('change', addPatientImageItems);
  rows.forEach(row => {
    row
      .querySelector('.delete')
      .addEventListener('click', deletePatientImageItem);
    row.querySelector('.labeling').addEventListener('click', openLabelingPopup);
  });
}

async function createPatientImageItem(file) {
  const row = document.querySelector('.row.example');
  const patientImageItem = document.createElement('div');
  const id = increasePatientImageCount();

  patientImageItem.classList.add('row');
  patientImageItem.dataset.id = id;
  patientImageItem.innerHTML = row.innerHTML;

  const fp = new FileParser(file);
  const exif = await fp.getExif();
  const img = patientImageItem.querySelector('img.original');
  const editImg = patientImageItem.querySelector('img.edit');

  const deletePatientImageItem = e => {
    patientImageItem.remove();
  };

  const openLabelingPopup = () => {
    editor.labeling.openPopup(img, id);
  };

  img.src = await fp.getDataURL();
  editImg.src = img.src;
  img.dataset.name = fp.getOriginalname();

  patientImageItem.querySelector('input[name=shootingDate]').value =
    exif.DateTimeOriginal;
  patientImageItem.querySelector(
    'input[name=hospitalId]'
  ).value = fp.getHospitalId();
  patientImageItem.querySelector(
    'input[name=patientNum]'
  ).value = fp.getPatientNum();
  patientImageItem.querySelector('input[name=shootingEquipment]').value =
    exif.Make;

  patientImageItem
    .querySelector('.delete')
    .addEventListener('click', deletePatientImageItem);
  patientImageItem
    .querySelector('.labeling')
    .addEventListener('click', openLabelingPopup);

  return patientImageItem;
}

function increasePatientImageCount() {
  return ++editor.patientImageCount;
}

if (location.pathname.indexOf('/editor') !== -1) init();

function getLablingImages() {
  const labelingImages = document.querySelectorAll('.labeling-image');
  const labelingFiles = [];
  const labelingInfo = [];

  labelingImages.forEach((labelingImage, key) => {
    const { x, y, width, height, desc, parent, no } = labelingImage.dataset;
    const patientImage = document.querySelector(`.row[data-id='${parent}']`);

    labelingInfo.push({
      treatmentDate: patientImage.querySelector('input[name=shootingDate]')
        .value,
      hospitalId: patientImage.querySelector('input[name=hospitalId]').value,
      patientNum: patientImage.querySelector('input[name=patientNum]').value,
      description: desc,
      posX: x,
      posY: y,
      height,
      width,
      parent,
      no,
      photoNum: patientImage.dataset.photonum
    });

    const filename = patientImage.querySelector('.original').dataset.name;

    labelingFiles.push(
      FileParser.dataURLToFile(
        labelingImage.src,
        `${getOriginalname(filename)}_${key}.JPG`
      )
    );
  });

  return { labelingFiles, labelingInfo };
}

function getPatientImages() {
  const rows = document.querySelectorAll('.row:not(.example)');
  const originalImages = [],
    editImages = [];
  rows.forEach(row => {
    const originalImage = row.querySelector('img.original');
    const editImage = row.querySelector('img.edit');
    originalImages.push(
      FileParser.dataURLToFile(originalImage.src, originalImage.dataset.name)
    );

    editImages.push(
      FileParser.dataURLToFile(
        editImage.src,
        `${getOriginalname(originalImage.dataset.name)}_edit.JPG`
      )
    );
  });
  return [originalImages, editImages];
}

function getPatientImageInfo() {
  const patientImages = document.querySelectorAll('.row:not(.example)');
  const patientImageInfo = [];

  patientImages.forEach(patientImage => {
    console.log(patientImage.querySelector('[name=treatmentProgress]').value);
    const originalImage = patientImage.querySelector('img.original');
    patientImageInfo.push({
      shootingDate: patientImage.querySelector('input[name=shootingDate]')
        .value,
      shootingEquipment: patientImage.querySelector(
        'input[name=shootingEquipment]'
      ).value,
      bodyPartId: patientImage.querySelector('select[name=bodyPartId]').value,
      isOperation: patientImage.querySelector('input[name=isOperation]')
        .checked,
      isBiopsy: patientImage.querySelector('input[name=isBiopsy]').checked,
      isLaser: patientImage.querySelector('input[name=isLaser]').checked,
      treatmentProgress: patientImage.querySelector('[name=treatmentProgress]')
        .value,
      hospitalId: patientImage.querySelector('input[name=hospitalId]').value,
      patientNum: patientImage.querySelector('input[name=patientNum]').value,
      _id: patientImage.dataset.id,
      photoNum: patientImage.dataset.photonum,
      originalName: originalImage.dataset.name,
      edit: patientImage.dataset.edit
    });
  });

  return patientImageInfo;
}
