import axios from 'axios';
import moment from 'moment';

(function() {
  const editClickHandle = e => {
    const { treatmentdate, patientnum, hospitalid } = e.target.dataset;
    location.href = `/editor?treatmentDate=${moment(
      treatmentdate,
      'YYYYMMDD'
    ).format('YYYY-MM-DD')}&patientNum=${patientnum}&hospitalId=${hospitalid}`;
  };

  const deleteClickHandle = async e => {
    if (!confirm('삭제하시겠습니까?')) return;
    const { treatmentdate, patientnum, hospitalid } = e.target.dataset;

    const response = await axios.delete(
      `/api/treatment?treatmentDate=${treatmentdate}&patientNum=${patientnum}&hospitalId=${hospitalid}`
    );
    if (response.status === 200) {
      alert('삭제되었습니다');
      location.href = '/status';
    }
  };

  if (document.querySelector('.js-treatment-edit')) {
    document
      .querySelector('.js-treatment-edit')
      .addEventListener('click', editClickHandle);
  }

  if (document.querySelector('.js-treatment-delete')) {
    document
      .querySelector('.js-treatment-delete')
      .addEventListener('click', deleteClickHandle);
  }
})();
