import moment from 'moment';
import Exif from 'exif-js';

function FileParser(file) {
  this.file = file;
}

FileParser.prototype.getFile = function() {
  return this.file;
};

FileParser.prototype.setFile = function(file) {
  this.file = file;
};

FileParser.prototype.getOriginalname = function() {
  let [originalname, extension] = this.file.name.split('.');
  if (originalname.indexOf('/') !== -1) {
    originalname = originalname.split('/')[1];
  }
  return originalname;
};

FileParser.prototype.getHospitalId = function() {
  const [hospitalId, num] = this.getOriginalname().split('-');
  return hospitalId;
};

FileParser.prototype.getPatientNum = function() {
  let [hospitalId, num] = this.getOriginalname().split('-');
  if (num.indexOf('(') !== -1) num = num.split(' ')[0];
  return num;
};

FileParser.prototype.getExif = function() {
  return new Promise(resolve => {
    Exif.getData(this.file, function() {
      resolve({
        Make: Exif.getTag(this, 'Make'),
        DateTimeOriginal: moment(
          Exif.getTag(this, 'DateTimeOriginal'),
          'YYYY:MM:DD hh:mm:ss'
        ).format('YYYY-MM-DD hh:mm:ss')
      });
    });
  });
};

FileParser.prototype.getDataURL = function() {
  return new Promise(resolve => {
    const fileReader = new FileReader();
    fileReader.onload = function(e) {
      resolve(e.target.result);
    };

    fileReader.readAsDataURL(this.file);
  });
};

FileParser.dataURLToFile = function(dataURL, filename) {
  try {
    let arr = dataURL.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  } catch (error) {
    return null;
  }
};

export default FileParser;
