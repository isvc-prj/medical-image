import Exif from 'exif-js';
import moment from 'moment';

export const getOriginalname = name => {
  let [originalname, extension] = name.split('.');
  if (originalname.indexOf('/') !== -1) {
    originalname = originalname.split('/')[1];
  }

  return originalname;
};

export const getHospitalId = name => {
  try {
    const [hospitalId, num] = getOriginalname(name).split('-');
    return hospitalId;
  } catch (error) {
    console.error(error);
    return;
  }
};

export const getPatientNum = name => {
  try {
    let [hospitalId, num] = getOriginalname(name).split('-');
    if (num.indexOf('(') !== -1) num = num.split(' ')[0];
    return num;
  } catch (error) {
    console.error(error);
    return;
  }
};

export const getExif = file => {
  return new Promise(resolve => {
    Exif.getData(file, function() {
      resolve({
        Make: Exif.getTag(this, 'Make'),
        DateTimeOriginal: moment(
          Exif.getTag(this, 'DateTimeOriginal'),
          'YYYY:MM:DD hh:mm:ss'
        ).format('YYYY-MM-DD hh:mm:ss')
      });
    });
  });
};

export const toggleLoader = () => {
  document.querySelector('.loader-wrapper').classList.toggle('hidden');
};

export const isBase64 = str => str.indexOf('data:image/jpeg;base64') !== -1;

export const setProgress = (value, _text, _desc) => {
  const progress = document.querySelector('.loader-progress');
  const text = document.querySelector('.loader-text');
  const desc = document.querySelector('.loader-desc');
  progress.style.width = parseInt(value) + '%';
  if (_text) text.textContent = _text;
  if (_desc) desc.textContent = _desc;
};

export const getBase64Image = img => {
  const canvas = document.createElement('canvas');
  canvas.width = img.width;
  canvas.height = img.height;
  const ctx = canvas.getContext('2d');
  ctx.drawImage(img, 0, 0);
  const dataURL = canvas.toDataURL();
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
};

export const toggleLoaderCircle = () => {
  document.querySelector('.loader-wrapper-circle').classList.toggle('hidden');
};
