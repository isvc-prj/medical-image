import axios from 'axios';
import { toggleLoaderCircle } from './utils';
import Labeling from './Labeling';
import FileParser from './FileParser';
import moment from 'moment';

function OneRegistration() {
  this.form = document.querySelector('.info.patient');
  this.labeling = new Labeling();
  this.patientImageList = document.querySelector('.patient-images');
  this.patientImageCount = 0;

  this.init();
}

OneRegistration.prototype.init = function() {
  const rows = document.querySelectorAll('.row:not(.example)');

  const deletePatientImageItem = e => {
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
  };

  const openLabelingPopup = e => {
    const row =
      e.target.parentElement.parentElement.parentElement.parentElement;
    const img = row.querySelector('img');
    const id = row.dataset.id;
    this.labeling.openPopup(img, id);
  };

  rows.forEach(row => {
    row
      .querySelector('.delete')
      .addEventListener('click', deletePatientImageItem);
    row.querySelector('.labeling').addEventListener('click', openLabelingPopup);
  });

  const addPatientImageItems = async e => {
    if (e.target.files && e.target.files[0]) {
      const uploadFiles = e.target.files;
      this.setPatientNum(uploadFiles[0]);
      for (let i = 0; i < uploadFiles.length; i++) {
        const patientImageItem = await this.createPatientImageItem(
          uploadFiles[i]
        );
        this.patientImageList.appendChild(patientImageItem);
      }
    }
  };

  const uploadTreatment = async e => {
    e.preventDefault();

    const formData = new FormData(this.form);
    const [originalImages, editImages] = this.getPatientImages();
    const info = this.getPatientImageInfo();
    const { labelingFiles, labelingInfo } = this.getLablingImages();

    originalImages.forEach(image => {
      formData.append('images[]', image);
    });
    info.forEach(info => {
      formData.append('imageInfo[]', JSON.stringify(info));
    });

    editImages.forEach(image => {
      formData.append('editImages[]', image);
    });

    labelingFiles.forEach(image => {
      formData.append('labelingImages[]', image);
    });
    labelingInfo.forEach(info => {
      formData.append('labelingInfo[]', JSON.stringify(info));
    });

    formData.append('photoCount', originalImages.length);

    toggleLoaderCircle();
    const response = await axios.post('/treatment', formData, {
      headers: { 'content-type': 'multipart/form-data' }
    });
    toggleLoaderCircle();

    if (response.status === 200) {
      if (response.data.treatment.identifiers) {
        const {
          treatmentDate,
          patientNum,
          hospitalId
        } = response.data.treatment.identifiers[0];
        location.href = `/status-detail?treatmentDate=${moment(
          treatmentDate,
          'YYYYMMDD'
        ).format(
          'YYYY-MM-DD'
        )}&patientNum=${patientNum}&hospitalId=${hospitalId}`;
      } else {
        const {
          treatmentDate,
          patientNum,
          hospitalId
        } = response.data.treatment;
        if (response.data.exist) alert('이미 존재하는 진료 정보입니다.');
        location.href = `/status-detail?treatmentDate=${treatmentDate}&patientNum=${patientNum}&hospitalId=${hospitalId}`;
      }
    }
  };

  document
    .querySelector('.image-selector')
    .addEventListener('change', addPatientImageItems);
  document
    .querySelector('.info.patient')
    .addEventListener('submit', uploadTreatment);
};

OneRegistration.prototype.getLablingImages = function() {
  const labelingImages = document.querySelectorAll('.labeling-image');
  const labelingFiles = [];
  const labelingInfo = [];

  labelingImages.forEach((labelingImage, key) => {
    const { x, y, width, height, desc, parent } = labelingImage.dataset;
    const patientImage = document.querySelector(`.row[data-id='${parent}']`);

    labelingInfo.push({
      treatmentDate: patientImage.querySelector('input[name=shootingDate]')
        .value,
      hospitalId: patientImage.querySelector('input[name=hospitalId]').value,
      patientNum: patientImage.querySelector('input[name=patientNum]').value,
      description: desc,
      posX: x,
      posY: y,
      height,
      width,
      parent
    });

    const filename = document.querySelector(
      `.row[data-id='${parent}'] .original`
    ).dataset.name;

    labelingFiles.push(
      FileParser.dataURLToFile(labelingImage.src, `${filename}_${key}.JPG`)
    );
  });

  return { labelingFiles, labelingInfo };
};

OneRegistration.prototype.getPatientImages = function() {
  const rows = document.querySelectorAll('.row:not(.example)');
  const originalImages = [],
    editImages = [];

  rows.forEach(row => {
    const originalImage = row.querySelector('img.original');
    const editImage = row.querySelector('img.edit');

    originalImages.push(
      FileParser.dataURLToFile(
        originalImage.src,
        `${originalImage.dataset.name}.JPG`
      )
    );

    editImages.push(
      FileParser.dataURLToFile(
        editImage.src,
        `${originalImage.dataset.name}_edit.JPG`
      )
    );
  });
  return [originalImages, editImages];
};

OneRegistration.prototype.getPatientImageInfo = function() {
  const patientImages = document.querySelectorAll('.row:not(.example)');
  const patientImageInfo = [];

  patientImages.forEach(patientImage => {
    patientImageInfo.push({
      filename: patientImage.dataset.filename,
      shootingDate: patientImage.querySelector('input[name=shootingDate]')
        .value,
      shootingEquipment: patientImage.querySelector(
        'input[name=shootingEquipment]'
      ).value,
      bodyPartId: patientImage.querySelector('select[name=bodyPartId]').value,
      isOperation: patientImage.querySelector('input[name=isOperation]')
        .checked,
      isBiopsy: patientImage.querySelector('input[name=isBiopsy]').checked,
      isLaser: patientImage.querySelector('input[name=isLaser]').checked,
      treatmentProgress: patientImage.querySelector('[name=treatmentProgress]')
        .value,
      hospitalId: patientImage.querySelector('input[name=hospitalId]').value,
      patientNum: patientImage.querySelector('input[name=patientNum]').value,
      _id: patientImage.dataset.id
    });
  });

  return patientImageInfo;
};

OneRegistration.prototype.setPatientNum = function(file) {
  const fileParser = new FileParser(file);
  this.form.querySelector(
    'input[name=patientNum]'
  ).value = fileParser.getPatientNum();
};

OneRegistration.prototype.createPatientImageItem = async function(file) {
  const row = document.querySelector('.row.example');
  const patientImageItem = document.createElement('div');
  const id = this.increasePatientImageCount();

  patientImageItem.classList.add('row');
  patientImageItem.dataset.id = id;
  patientImageItem.dataset.filename = file.name;
  patientImageItem.innerHTML = row.innerHTML;

  const deletePatientImageItem = e => {
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
  };

  const openLabelingPopup = e => {
    const row =
      e.target.parentElement.parentElement.parentElement.parentElement;
    const img = row.querySelector('img');
    const id = row.dataset.id;
    this.labeling.openPopup(img, id);
  };

  const fp = new FileParser(file);
  const exif = await fp.getExif();
  const img = patientImageItem.querySelector('img.original');
  const editImg = patientImageItem.querySelector('img.edit');

  img.src = await fp.getDataURL();
  editImg.src = img.src;
  img.dataset.name = fp.getOriginalname();

  patientImageItem.querySelector('input[name=shootingDate]').value =
    exif.DateTimeOriginal;
  patientImageItem.querySelector(
    'input[name=hospitalId]'
  ).value = fp.getHospitalId();
  patientImageItem.querySelector(
    'input[name=patientNum]'
  ).value = fp.getPatientNum();
  patientImageItem.querySelector('input[name=shootingEquipment]').value =
    exif.Make;

  patientImageItem
    .querySelector('.delete')
    .addEventListener('click', deletePatientImageItem);
  patientImageItem
    .querySelector('.labeling')
    .addEventListener('click', openLabelingPopup);

  return patientImageItem;
};

OneRegistration.prototype.increasePatientImageCount = function() {
  return ++this.patientImageCount;
};

if (location.pathname.indexOf('/one-registration') !== -1)
  new OneRegistration();
