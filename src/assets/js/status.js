import axios from 'axios';
import queryString from 'querystring';

(function() {
  const imagesElement = document.querySelector('.images');

  function init() {
    if (document.querySelector('.treatment')) {
      document.querySelectorAll('.treatment').forEach(treatment => {
        const { treatmentdate, patientnum, hospitalid } = treatment.dataset;

        treatment.addEventListener('dblclick', () => {
          const { hospitalId } = queryString.parse(
            location.search.replace('?', '')
          );
          location.href = `/status-detail?${location.search.replace('?', '')}${
            hospitalId ? '' : `&hospitalId=${hospitalid}`
          }&treatmentDate=${treatmentdate}&patientNum=${patientnum}`;
        });

        treatment.addEventListener('click', e => {
          const element = document.querySelector('.treatment.active');
          if (element) element.classList.remove('active');
          e.currentTarget.classList.add('active');
          axios
            .get(
              `/api/photos?treatmentDate=${treatmentdate}&patientNum=${patientnum}&hospitalId=${hospitalid}`
            )
            .then(response => {
              let html = '';
              imagesElement.innerHTML = html;
              response.data.forEach(photo => {
                const { hospitalId } = queryString.parse(
                  location.search.replace('?', '')
                );

                html += `
                  <a href="/status-detail?${location.search.replace('?', '')}${
                  hospitalId ? '' : `&hospitalId=${hospitalid}`
                }&treatmentDate=${treatmentdate}&patientNum=${patientnum}">
                    <img src='${
                      photo.editFile ? photo.editFile : photo.originalFile
                    }'/>
                  </a>`;
              });
              imagesElement.innerHTML = html;
            });
        });
      });

      const firstRow = document.querySelector('.treatment');
      firstRow.classList.add('active');
      firstRow.click();
    }

    if (document.querySelector('.back-button')) {
      document.querySelector('.back-button').addEventListener('click', () => {
        const {
          diagnosisId,
          endAt,
          hospitalId,
          startAt,
          page
        } = queryString.parse(location.search.replace('?', ''));

        location.href = `/status?${startAt ? `&startAt=${startAt}` : ''}${
          endAt ? `&endAt=${endAt}` : ''
        }${hospitalId ? `&hospitalId=${hospitalId}` : ''}${
          diagnosisId ? `&diagnosisId=${diagnosisId}` : ''
        }${page ? `&page=${page}` : ''}`;
      });
    }
  }

  init();
})();
