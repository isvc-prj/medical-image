import '@babel/polyfill';

import '../sass/style.scss';

import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/menu.css';
import 'jquery-ui/themes/base/theme.css';
import 'jquery-ui/themes/base/datepicker.css';

import './common';
import './one-registration';
import './batch-registration';
import './status';
import './status-detail';
import './data';
import './editor';
