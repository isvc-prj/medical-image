import {
  getHospitalId,
  getExif,
  getPatientNum,
  toggleLoader,
  setProgress,
  toggleLoaderCircle
} from './utils';
import axios from 'axios';

let formData;
let dirSelected = false,
  xlsxSelected = false;

(function() {
  const dirUploadBtn = document.querySelector('.directory-upload');
  const xlsxUploadForm = document.querySelector('.js-xlsx-upload-form');

  const dirUploadHandle = async () => {
    if (formData && dirSelected) {
      toggleLoader();
      const response = await axios.post('/dir', formData, {
        headers: { 'content-type': 'multipart/form-data' },
        onUploadProgress: progressEvent => {
          if (progressEvent.lengthComputable) {
            setProgress(
              Math.ceil((progressEvent.loaded / progressEvent.total) * 100),
              Math.ceil((progressEvent.loaded / progressEvent.total) * 100) +
                '%',
              '업로드 중...'
            );
            if ((progressEvent.loaded / progressEvent.total) * 100 >= 100) {
              setProgress(100, '100%', '업로드 중...');
            }
          }
        }
      });
      if (response.status === 200) {
        alert('업로드가 완료되었습니다.');
        location.href = '/batch-registration';
      }
    } else {
      alert('폴더를 선택해 주세요.');
    }
  };

  const dirChangeHandle = async e => {
    formData = new FormData();
    if (e.target.files && e.target.files[0]) {
      toggleLoader();
      dirSelected = true;
      for (let index = 0; index < e.target.files.length; index++) {
        setProgress(
          Math.ceil((index / e.target.files.length) * 100),
          Math.ceil((index / e.target.files.length) * 100) + '%',
          '파일을 해석하는 중...'
        );
        if (getPatientNum(e.target.files[index].name) != undefined) {
          const info = await getExif(e.target.files[index]);
          info.hospitalId = getHospitalId(e.target.files[index].name);
          info.patientNum = getPatientNum(e.target.files[index].name);
          info.filename = e.target.files[index].name;
          formData.append('images[]', e.target.files[index]);
          formData.append('imageInfo[]', JSON.stringify(info));
        }
      }
      toggleLoader();
    }
  };

  const xlsxUpload = async e => {
    e.preventDefault();
    if (!xlsxSelected) {
      alert('파일을 선택하세요.');
      return;
    }
    toggleLoader();
    formData = new FormData(xlsxUploadForm);

    const response = await axios
      .post('/xlsx', formData, {
        headers: { 'content-type': 'multipart/form-data' },
        onUploadProgress: progressEvent => {
          if (progressEvent.lengthComputable) {
            setProgress(
              Math.ceil((progressEvent.loaded / progressEvent.total) * 100),
              Math.ceil((progressEvent.loaded / progressEvent.total) * 100) +
                '%',
              '업로드 중...'
            );
            if ((progressEvent.loaded / progressEvent.total) * 100 >= 100) {
              setProgress(100, '100%', '업로드 중...');
            }
          }
        }
      })
      .catch(reason => {
        alert('업로드에 실패하였습니다.');
        toggleLoader();
      });
    if (response.status === 200) {
      alert('업로드가 완료되었습니다.');
      location.href = '/batch-registration';
    }
    //toggleLoaderCircle();
  };

  const xlsxChangeHandle = e => {
    if (e.target.files && e.target.files[0]) {
      xlsxSelected = true;
    }
  };

  function init() {
    if (dirUploadBtn) {
      dirUploadBtn.addEventListener('click', dirUploadHandle);
      document
        .querySelector('input[name=directory]')
        .addEventListener('change', dirChangeHandle);

      document
        .querySelector('.xlsx-upload')
        .addEventListener('click', xlsxUpload);

      document
        .querySelector('input[name=xlsx]')
        .addEventListener('change', xlsxChangeHandle);
    }
  }

  init();
})();
