const { isBase64 } = require('./utils');

function Labeling() {
  this.popup = document.querySelector('.popup-background');
  this.crops = this.popup.querySelector('.crops');
  this.canvas = this.popup.querySelector('.labeling-target-image');
  this.list = this.popup.querySelector('.labeling-list');
  this.saveBtn = this.popup.querySelector('button.save');
  this.closeBtn = this.popup.querySelector('button.close');

  this.context = this.canvas.getContext('2d');

  this.startPoint = null;
  this.cropId = 0;
  this.targetId = null;

  this.init();
}

Labeling.prototype.getAllCropBox = function() {
  return document.querySelectorAll('.crop');
};

Labeling.prototype.init = function() {
  const deleteCropBox = e => {
    if (e.keyCode === 46) {
      this.deleteCropBox();
    }
  };

  const resizeCropBox = e => {
    const currentPoint = { x: e.offsetX, y: e.offsetY };
    const selectedCrop = this.getSelectedCropBox();

    selectedCrop.style.width =
      Math.abs(currentPoint.x - this.startPoint.x) + 'px';
    selectedCrop.style.height =
      Math.abs(currentPoint.y - this.startPoint.y) + 'px';
    selectedCrop.style.left =
      Math.min(
        currentPoint.x + this.canvas.offsetLeft,
        this.startPoint.x + this.canvas.offsetLeft
      ) + 'px';
    selectedCrop.style.top = Math.min(currentPoint.y, this.startPoint.y) + 'px';
  };

  const finishAddCropBox = () => {
    const selectedCrop = this.getSelectedCropBox();
    this.getAllCropBox().forEach(
      crop => (crop.style.pointerEvents = 'visible')
    );
    //selectedCrop.style.pointerEvents = 'visible';
    this.addListItem(selectedCrop);

    this.canvas.removeEventListener('mousemove', resizeCropBox);
    this.canvas.removeEventListener('mouseup', finishAddCropBox);
  };

  const startAddCropBox = e => {
    this.setStartPoint(e.offsetX, e.offsetY);
    const newCropBox = this.createCropBox(e.offsetX, e.offsetY);
    this.getAllCropBox().forEach(crop => (crop.style.pointerEvents = 'none'));
    this.addCropBox(newCropBox);

    this.canvas.addEventListener('mousemove', resizeCropBox);
    this.canvas.addEventListener('mouseup', finishAddCropBox);
  };

  const closePopup = () => {
    this.reset();
    this.popup.classList.add('hidden');
  };

  const saveListItems = () => {
    const items = this.list.querySelectorAll('li');

    const row = document.querySelector(`.row[data-id='${this.targetId}']`);
    const labelingListCell = document.querySelector(
      `.row[data-id='${this.targetId}'] .labeling-list-cell`
    );

    const textareas = document.querySelectorAll('textarea');

    let isEmpty = false;

    textareas.forEach(textarea => {
      if (textarea.value === '') isEmpty = true;
    });

    if (isEmpty) {
      alert('빈 항목이 있습니다.');
      return;
    }
    labelingListCell.innerHTML = '';
    items.forEach(item => {
      const labelingImage = this.createLabelingImageItem(item);
      labelingListCell.appendChild(labelingImage);
      this.drawCropBox(item);
    });

    this.setEditImage(this.canvas.toDataURL('image/jpg'));
    row.dataset.edit = true;
    closePopup();
  };

  document.addEventListener('keyup', deleteCropBox);
  this.canvas.addEventListener('mousedown', startAddCropBox);
  this.closeBtn.addEventListener('click', closePopup);
  this.saveBtn.addEventListener('click', saveListItems);
};

Labeling.prototype.getSelectedCropBox = function() {
  return document.querySelector('.crop.selected');
};

Labeling.prototype.setEditImage = function(dataURL) {
  const originalImage = document.querySelector(
    `.row[data-id='${this.targetId}'] img.original`
  );
  const editImage = document.querySelector(
    `.row[data-id='${this.targetId}'] img.edit`
  );
  editImage.src = dataURL;
  editImage.classList.remove('hidden');
  originalImage.classList.add('hidden');
};

Labeling.prototype.drawCropBox = function(item) {
  const { x, y, width, height } = item.dataset;

  this.context.strokeStyle = 'blue';
  this.context.lineWidth = '3px';
  this.context.strokeRect(x, y, width, height);
};

Labeling.prototype.createLabelingImageItem = function(item) {
  const canvas = item.querySelector('canvas');
  const textarea = item.querySelector('textarea');

  const div = document.createElement('div');
  const img = document.createElement('img');
  const text = document.createElement('div');
  div.classList.add('labeling-item');
  img.classList.add('labeling-image');
  img.dataset.parent = this.targetId;
  img.dataset.x = item.dataset.x;
  img.dataset.y = item.dataset.y;
  img.dataset.width = item.dataset.width;
  img.dataset.height = item.dataset.height;
  img.dataset.desc = textarea.value;
  img.dataset.no = item.dataset.no;

  const dataURL = canvas.toDataURL('image/jpg');
  img.src = dataURL;
  text.textContent = textarea.value;

  div.appendChild(img);
  div.appendChild(text);

  return div;
};

Labeling.prototype.deleteCropBox = function() {
  const selectedCrop = this.getSelectedCropBox();
  if (selectedCrop) {
    this.deleteListItem(selectedCrop.dataset.id);
    selectedCrop.remove();
  }
};

Labeling.prototype.focusoutCropBox = function() {
  const selectedCrop = this.getSelectedCropBox();
  if (selectedCrop) selectedCrop.classList.remove('selected');
};

Labeling.prototype.createCropBox = function(
  x,
  y,
  width = 1,
  height = 1,
  desc,
  no
) {
  const newCropBox = document.createElement('div');
  const { offsetLeft } = this.canvas;

  newCropBox.classList.add('crop', 'selected');
  newCropBox.dataset.id = this.getCropId();
  newCropBox.dataset.desc = desc || '';
  newCropBox.dataset.no = no || '';

  newCropBox.style.width = width + 'px';
  newCropBox.style.height = height + 'px';
  newCropBox.style.left = x + offsetLeft + 'px';
  newCropBox.style.top = y + 'px';

  const focusCropBox = e => {
    this.focusoutCropBox();

    e.target.classList.add('selected');
  };

  const dragCropBox = e => {
    const selectedCrop = e.target;

    selectedCrop.style.left =
      selectedCrop.offsetLeft - (this.startPoint.x - e.offsetX) + 'px';
    selectedCrop.style.top =
      selectedCrop.offsetTop - (this.startPoint.y - e.offsetY) + 'px';

    this.updateListItem(selectedCrop);
  };

  const endDragCropBox = () => {
    newCropBox.removeEventListener('mousemove', dragCropBox);
    newCropBox.removeEventListener('mouseup', endDragCropBox);
  };

  const startDragCropBox = e => {
    if (this.getSelectedCropBox() !== e.target) focusCropBox(e);

    this.setStartPoint(e.offsetX, e.offsetY);

    newCropBox.addEventListener('mousemove', dragCropBox);
    newCropBox.addEventListener('mouseup', endDragCropBox);
  };

  newCropBox.addEventListener('click', focusCropBox);
  newCropBox.addEventListener('mousedown', startDragCropBox);

  return newCropBox;
};

Labeling.prototype.addCropBox = function(cropBox, visible) {
  this.focusoutCropBox();

  if (visible) cropBox.style.pointerEvents = 'visible';

  this.crops.appendChild(cropBox);
};

Labeling.prototype.setStartPoint = function(x, y) {
  this.startPoint = { x, y };
};

Labeling.prototype.setCanvasSize = function(width, height) {
  this.canvas.width = width;
  this.canvas.height = height;
};

Labeling.prototype.drawImage = async function(img) {
  return new Promise(resolve => {
    const image = new Image();
    image.onload = () => {
      this.setCanvasSize(image.width, image.height);
      this.context.drawImage(image, 0, 0, image.width, image.height);
      resolve();
    };
    if (isBase64(img.src)) image.src = img.src;
    else
      $.ajax({
        url: img.src,
        cache: false,
        xhr: function() {
          var xhr = new XMLHttpRequest();
          xhr.responseType = 'blob';
          return xhr;
        },
        success: function(data) {
          var url = window.URL || window.webkitURL;
          image.src = url.createObjectURL(data);
        },
        error: function() {}
      });
  });
};

Labeling.prototype.openPopup = async function(img, id) {
  await this.drawImage(img);
  this.targetId = id;

  this.popup.classList.remove('hidden');
  this.setCropAndList();
};

Labeling.prototype.setCropAndList = function() {
  const labelingItems = document.querySelectorAll(
    `.row[data-id='${this.targetId}'] .labeling-list-cell .labeling-item`
  );
  labelingItems.forEach(item => {
    const image = item.querySelector('img');
    const { x, y, width, height, desc, no } = image.dataset;
    const ratioX = this.canvas.width / this.canvas.offsetWidth;
    const ratioY = this.canvas.height / this.canvas.offsetHeight;

    const newCropBox = this.createCropBox(
      parseInt(x) / ratioX,
      parseInt(y) / ratioY,
      parseInt(width) / ratioX,
      parseInt(height) / ratioY,
      desc,
      no
    );
    this.addCropBox(newCropBox, true);
    this.addListItem(newCropBox);
  });
};

Labeling.prototype.getCropId = function() {
  return ++this.cropId;
};

Labeling.prototype.addListItem = function(cropBox) {
  const listItem = this.createListItem(cropBox);
  this.list.appendChild(listItem);
};

Labeling.prototype.createListItem = function(cropBox) {
  const li = document.createElement('li');
  const div = document.createElement('div');
  const canvas = document.createElement('canvas');
  const textarea = document.createElement('textarea');
  const { desc, no } = cropBox.dataset;

  textarea.innerHTML = desc !== 'undefined' ? desc : '';

  const imageData = this.getCropImage(cropBox);
  this.setCropImage(canvas, imageData);

  const cropLocation = this.getCropLocation(cropBox);

  li.dataset.x = cropLocation.x;
  li.dataset.y = cropLocation.y;
  li.dataset.width = cropLocation.width;
  li.dataset.height = cropLocation.height;
  li.dataset.no = no;

  div.classList.add('image-wrapper');
  li.dataset.id = cropBox.dataset.id;

  div.appendChild(canvas);
  li.appendChild(div);
  li.appendChild(textarea);

  return li;
};

Labeling.prototype.deleteListItem = function(id) {
  const li = this.list.querySelector(`li[data-id='${id}']`);
  if (li) li.remove();
};

Labeling.prototype.getCropImage = function(cropBox) {
  const { x, y, width, height } = this.getCropLocation(cropBox);

  return this.context.getImageData(x, y, width, height);
};

Labeling.prototype.getCropLocation = function(cropBox) {
  const { left, top, width, height } = cropBox.style;

  const ratioX = this.canvas.width / this.canvas.offsetWidth;
  const ratioY = this.canvas.height / this.canvas.offsetHeight;

  return {
    x: (parseInt(left) - this.canvas.offsetLeft) * ratioX,
    y: parseInt(top) * ratioY,
    width: parseInt(width) * ratioX,
    height: parseInt(height) * ratioY
  };
};

Labeling.prototype.setCropImage = function(canvas, imageData) {
  const context = canvas.getContext('2d');

  canvas.width = imageData.width;
  canvas.height = imageData.height;
  context.putImageData(imageData, 0, 0);
};

Labeling.prototype.updateListItem = function(cropBox) {
  const li = this.list.querySelector(`li[data-id='${cropBox.dataset.id}']`);

  const canvas = li.querySelector('canvas');
  const imageData = this.getCropImage(cropBox);
  const cropLocation = this.getCropLocation(cropBox);

  this.setCropImage(canvas, imageData);
  li.dataset.x = cropLocation.x;
  li.dataset.y = cropLocation.y;
  li.dataset.width = cropLocation.width;
  li.dataset.height = cropLocation.height;
};

Labeling.prototype.reset = function() {
  this.list.innerHTML = '';
  this.crops.innerHTML = '';
  this.cropId = 0;
};

module.exports = Labeling;
