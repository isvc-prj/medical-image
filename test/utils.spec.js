import { getPatientNum, getHospitalId } from '../src/assets/js/utils';
import moment from 'moment';

describe('getPatientNum test', () => {
  it('should change BRMH-001.JPG to 001', done => {
    if (getPatientNum('BRMH-001.JPG') === '001') {
      done();
    }
  });

  it('should change dir/BRMH-001.JPG to 001', done => {
    if (getPatientNum('dir/BRMH-001.JPG') === '001') {
      done();
    }
  });

  it('should change dir/BRMHAS-001.JPG to 001', done => {
    if (getPatientNum('dir/BRMHAS-001.JPG') === '001') {
      done();
    }
  });

  it('should change BRMHAS-001.JPG to 001', done => {
    if (getPatientNum('BRMHAS-001.JPG') === '001') {
      done();
    }
  });

  it('should change BRMHAS-001 (2).JPG to 001', done => {
    if (getPatientNum('BRMHAS-001 (2).JPG') === '001') {
      done();
    }
  });
});

describe('getHospitalId test', () => {
  it('should change BRMH-001.JPG to BRMH', done => {
    if (getHospitalId('BRMH-001.JPG') === 'BRMH') {
      done();
    }
  });

  it('should change dir/BRMH-001.JPG to BRMH', done => {
    if (getHospitalId('dir/BRMH-001.JPG') === 'BRMH') {
      done();
    }
  });

  it('should change dir/BRMHAS-001.JPG to BRMHAS', done => {
    if (getHospitalId('dir/BRMHAS-001.JPG') === 'BRMHAS') {
      done();
    }
  });

  it('should change BRMHAS-001.JPG to BRMHAS', done => {
    if (getHospitalId('BRMHAS-001.JPG') === 'BRMHAS') {
      done();
    }
  });
});

describe('moment test', () => {
  it('moment test 1', done => {
    if (
      moment('2019-03-06 09:50:36', 'YYYY-MM-DD hh:mm:ss').format(
        'YYYYMMDDhhmmss'
      ) === '20190306095036'
    ) {
      done();
    }
  });
});
